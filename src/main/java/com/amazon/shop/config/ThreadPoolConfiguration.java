package com.amazon.shop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池配置类
 *
 * @Author 郭非
 * @Date 2021/6/13
 * @Time 12:28
 * @Version 1.0
 */
@Configuration
public class ThreadPoolConfiguration {
    /**
     * 配置线程池
     *
     * @param poolProperties
     * @return
     */
    @Bean
    public ThreadPoolExecutor threadPoolExecutor(ThreadPoolProperties poolProperties) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                poolProperties.getCorePoolSize()
                , poolProperties.getMaxPoolSize()
                , poolProperties.getKeepAliveTime()
                , TimeUnit.DAYS
                , new LinkedBlockingQueue<>(10000)
                , Executors.defaultThreadFactory()
                , new ThreadPoolExecutor.AbortPolicy());
        return threadPoolExecutor;
    }
}
