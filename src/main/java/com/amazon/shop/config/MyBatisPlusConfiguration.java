package com.amazon.shop.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatisPlus配置类
 *
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 9:00
 * @Version 1.0
 */
@Configuration
public class MyBatisPlusConfiguration {
    /**
     * 分页插件
     *
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        //页数溢出自动返回第一页
        paginationInterceptor.setOverflow(true);
        //配置数据库类型
        paginationInterceptor.setDbType(DbType.MYSQL);
        //分页不限制条数
        paginationInterceptor.setLimit(-1);
        return paginationInterceptor;
    }

    /**
     * 乐观锁插件(备用)
     *
     * @return
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor() {
        return new OptimisticLockerInterceptor();
    }
}
