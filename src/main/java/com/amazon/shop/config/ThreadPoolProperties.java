package com.amazon.shop.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程属性配置类
 *
 * @Author 郭非
 * @Date 2021/6/13
 * @Time 12:30
 * @Version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "amazon.thread")
public class ThreadPoolProperties {
    /**
     * 核心线程数
     */
    private Integer corePoolSize;
    /**
     * 最大线程数
     */
    private Integer maxPoolSize;
    /**
     * 线程心跳时间
     */
    private Integer keepAliveTime;
}
