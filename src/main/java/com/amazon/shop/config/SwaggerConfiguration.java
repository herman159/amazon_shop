package com.amazon.shop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

/**
 * Swagger配置类
 *
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 9:05
 * @Version 1.0
 */
@Configuration
public class SwaggerConfiguration {
    /**
     * 创建Docket实例
     *
     * @return
     */
    @Bean
    public Docket docket() {
        //配置Swagger类型
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        //开启swagger
        docket.enable(true);
        //配置分组名
        docket.groupName("amazon_shop");
        //配置api信息
        docket.apiInfo(apiInfo());
        //配置搜索包
        docket
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.amazon.shop"))
                .build();
        return docket;
    }

    /**
     * 配置Api信息
     *
     * @return
     */
    public ApiInfo apiInfo() {
        return new ApiInfo(
                "Amazon Documentation"
                , "Amazon Documentation"
                , "1.0"
                , "urn:tos",
                contact()
                , "Apache 2.0"
                , "http://www.apache.org/licenses/LICENSE-2.0"
                , new ArrayList<VendorExtension>());
    }

    /**
     * 配置联系信息
     *
     * @return
     */
    public Contact contact() {
        return new Contact("youlixiang", "www.youlixiang.asia", "123@qq.com");
    }
}
