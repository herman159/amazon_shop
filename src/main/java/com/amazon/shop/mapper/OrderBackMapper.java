package com.amazon.shop.mapper;

import com.amazon.shop.entity.OrderBack;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-07-04
 */
public interface OrderBackMapper extends BaseMapper<OrderBack> {

}
