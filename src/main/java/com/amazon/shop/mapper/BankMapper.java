package com.amazon.shop.mapper;

import com.amazon.shop.entity.Bank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
public interface BankMapper extends BaseMapper<Bank> {

}
