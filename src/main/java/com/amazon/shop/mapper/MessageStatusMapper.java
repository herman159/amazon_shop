package com.amazon.shop.mapper;

import com.amazon.shop.entity.MessageStatus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-07-05
 */
public interface MessageStatusMapper extends BaseMapper<MessageStatus> {

}
