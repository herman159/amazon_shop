package com.amazon.shop.mapper;

import com.amazon.shop.entity.Company;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
public interface CompanyMapper extends BaseMapper<Company> {

}
