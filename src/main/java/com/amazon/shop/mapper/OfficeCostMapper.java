package com.amazon.shop.mapper;

import com.amazon.shop.entity.OfficeCost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
public interface OfficeCostMapper extends BaseMapper<OfficeCost> {

}
