package com.amazon.shop.mapper;

import com.amazon.shop.entity.BuyerInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
public interface BuyerInfoMapper extends BaseMapper<BuyerInfo> {

}
