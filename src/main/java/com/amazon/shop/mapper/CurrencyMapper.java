package com.amazon.shop.mapper;

import com.amazon.shop.entity.Currency;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-15
 */
public interface CurrencyMapper extends BaseMapper<Currency> {

}
