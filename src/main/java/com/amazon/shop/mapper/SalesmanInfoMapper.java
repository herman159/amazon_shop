package com.amazon.shop.mapper;

import com.amazon.shop.entity.SalesmanInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-07-01
 */
public interface SalesmanInfoMapper extends BaseMapper<SalesmanInfo> {

}
