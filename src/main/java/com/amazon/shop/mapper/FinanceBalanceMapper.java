package com.amazon.shop.mapper;

import com.amazon.shop.entity.FinanceBalance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
public interface FinanceBalanceMapper extends BaseMapper<FinanceBalance> {

}
