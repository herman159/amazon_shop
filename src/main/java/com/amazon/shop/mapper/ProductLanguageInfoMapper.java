package com.amazon.shop.mapper;

import com.amazon.shop.entity.ProductLanguageInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-16
 */
public interface ProductLanguageInfoMapper extends BaseMapper<ProductLanguageInfo> {

}
