package com.amazon.shop.mapper;

import com.amazon.shop.entity.Transfer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
public interface TransferMapper extends BaseMapper<Transfer> {

}
