package com.amazon.shop.mapper;

import com.amazon.shop.entity.Codes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-06-24
 */
public interface CodesMapper extends BaseMapper<Codes> {

}
