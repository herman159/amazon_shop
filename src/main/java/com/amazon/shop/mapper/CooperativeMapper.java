package com.amazon.shop.mapper;

import com.amazon.shop.entity.Cooperative;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
public interface CooperativeMapper extends BaseMapper<Cooperative> {

}
