package com.amazon.shop.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.dao.DataAccessException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 9:58
 * @Version 1.0
 */
@Data
public class TransferSearchVo {
    /**
     * 转账充值编号
     */
    private String transferId;
    /**
     * 转入账户
     */
    private String inAccount;
    /**
     * 转出账户
     */
    private String outAccount;
    /**
     * 金额
     */
    private BigDecimal money;
    /**
     * 备注
     */
    private String note;
    /**
     * 录入员
     */
    private String username;
    /**
     * 录入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    /**
     * 日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
}
