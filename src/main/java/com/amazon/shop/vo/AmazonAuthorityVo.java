package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/13
 * @Time 8:59
 * @Version 1.0
 */
@Data
public class AmazonAuthorityVo {
    /**
     * 站点编号
     */
    private String id;
    /**
     * AWS访问密钥编码
     */
    private String accessKeyId;
    /**
     * AWS访问密钥
     */
    private String secretKey;
    /**
     * IAM职权ARN
     */
    private String roleArn;
    /**
     * IAM职权名称
     */
    private String roleSessionName;
    /**
     * LWA客户端编码
     */
    private String clientId;
    /**
     * LWA客户端秘钥
     */
    private String clientSecret;
    /**
     * LWA授权服务器的节点地址
     */
    private String lwaEndPoint;
}
