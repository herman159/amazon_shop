package com.amazon.shop.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author 郭非
 * @Date 2021/6/28
 * @Time 11:00
 * @Version 1.0
 */
@Data
@NoArgsConstructor
public class AddressVo {
    /**
     * 地址
     */
    private String address;
    /**
     * 结构体
     */
    private Content content;
    /**
     * 返回状态码
     */
    private Integer status;

    /**
     * 结构体内部类
     */
    @Data
    @NoArgsConstructor
    public static class Content {
        /**
         * 地址
         */
        private String address;
        /**
         * 地址细节
         */
        private AddressDetail addressDetail;
        /**
         * 坐标
         */
        private Point point;
    }

    /**
     * 地址细节内部类
     */
    @Data
    @NoArgsConstructor
    public static class AddressDetail {
        /**
         * 城市
         */
        private String city;
        /**
         * 城市编号
         */
        private Integer cityCode;
        /**
         * 省份
         */
        private String province;
    }

    /**
     * 坐标内部类
     */
    @Data
    @NoArgsConstructor
    public static class Point {
        /**
         * x坐标
         */
        private String x;
        /**
         * y坐标
         */
        private String y;
    }
}
