package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 14:23
 * @Version 1.0
 */
@Data
public class TransferExpense {
    /**
     * 运单号
     */
    private String wayBillNumber;
    /**
     * 订单号
     */
    private String aboutOrder;
    /**
     * 业务员
     */
    private String username;
    /**
     * 关联公司
     */
    private String account;
    /**
     * 费用
     */
    private BigDecimal price;
    /**
     * 送货时间
     */
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date transferTime;
    /**
     * 物流员
     */
    private String salesman;
    /**
     * 到达时间
     */
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date comeTime;
    /**
     * 类型
     */
    private String type;
    /**
     * 部门
     */
    private String apartments;

}
