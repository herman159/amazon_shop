package com.amazon.shop.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/18
 * @Time 19:30
 * @Version 1.0
 */
@Data
public class SalesmanSaveVo {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 姓名
     */
    private String name;
    /**
     * qq
     */
    private String qq;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 权限列表
     */
    private List<String> permissionList;
    /**
     * 公司名称
     */
    private String company;
    /**
     * 角色
     */
    private String role;
}
