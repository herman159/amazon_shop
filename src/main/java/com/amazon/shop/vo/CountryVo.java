package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 20:04
 * @Version 1.0
 */
@Data
public class CountryVo {
    /**
     * 国家编号
     */
    private String id;
    /**
     * 国家名称
     */
    private String name;
    /**
     * 国家分组
     */
    private String countryGroup;
    /**
     * SP授权服务器节点地址
     */
    private String spEndpoint;
    /**
     * aws区域
     */
    private String aws;
    /**
     * 店铺地址编号
     */
    private String marketplaceId;
    /**
     * 国家地区代码
     */
    private String countryCode;
}
