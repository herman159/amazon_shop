package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/2
 * @Time 20:57
 * @Version 1.0
 */
@Data
public class OfficeCostSearchVo {
    /**
     * 主键编号
     */
    private String officeCostId;
    /**
     * 账户
     */
    private String account;
    /**
     * 用途
     */
    private String use;
    /**
     * 公司
     */
    private String company;
    /**
     * 支出
     */
    private BigDecimal expense;
    /**
     * 录入员
     */
    private String username;
    /**
     * 备注
     */
    private String note;
    /**
     * 录入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    /**
     * 日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
}
