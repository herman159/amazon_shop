package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/16
 * @Time 13:15
 * @Version 1.0
 */
@Data
public class OrderStatusVo {
    /**
     * 订单号
     */
    private String orderNumber;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 订单子状态
     */
    private String orderChildStatus;
}
