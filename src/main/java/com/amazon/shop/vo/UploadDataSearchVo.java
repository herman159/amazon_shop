package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/5
 * @Time 12:00
 * @Version 1.0
 */
@Data
public class UploadDataSearchVo {
    /**
     * 主键编号
     */
    private String uploadId;
    /**
     * 提要编号
     */
    private String feedId;
    /**
     * 业务员编号
     */
    private String salesman;
    /**
     * 数量
     */
    private String count;
    /**
     * 状态
     */
    private String status;
    /**
     * 错误
     */
    private String error;
    /**
     * 警告
     */
    private String warning;
    /**
     * 提交时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date submitTime;
    /**
     * 编号
     */
    private String amazonId;

}
