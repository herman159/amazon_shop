package com.amazon.shop.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/29
 * @Time 16:15
 * @Version 1.0
 */
@Data
public class CompanyVo {
    /**
     * 联系人
     */
    private String contacts;
    /**
     * qq
     */
    private String qq;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮箱
     */
    private String mail;
    /**
     * 物流邮箱
     */
    private String logisticsMail;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 公司简介
     */
    private String companyInfo;
    /**
     * 公司语言
     */
    private List<LanguageVo> languageVos;

}
