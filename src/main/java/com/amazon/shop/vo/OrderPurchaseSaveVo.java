package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;


@Data
public class OrderPurchaseSaveVo {
    /**
     * 商品图片
     */
    private ProductPicVo pixMap;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 变体
     */
    private String variant;

    /**
     * 转运商姓名
     */
    private String transportName;

    /**
     * qq
     */
    private String qq;

    /**
     * 转运中心
     */
    private String transportCenter;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 费用
     */
    private String money;

    /**
     * 追踪单号
     */
    private String trackNumber;

    /**
     * 签收人员
     */
    private String signName;

    /**
     * 入库时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date placeTime;

    /**
     * 检查类型
     */
    private String checkType;

    /**
     * 物品类型
     */
    private String productType;

    /**
     * 采购网站
     */
    private String purchaseWebsite;
    /**
     * 备注
     */
    private String note;
}
