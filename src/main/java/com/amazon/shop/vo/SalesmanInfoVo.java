package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/7/1
 * @Time 16:44
 * @Version 1.0
 */
@Data
public class SalesmanInfoVo {
    /**
     * 姓名
     */
    private String name;
    /**
     * 编号
     */
    private String number;
    /**
     * 登录用户名
     */
    private String loginName;
    /**
     * 登录密码
     */
    private String loginPassword;
    /**
     * qq
     */
    private String qq;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 部门
     */
    private String apartment;
    /**
     * 角色
     */
    private String role;
    /**
     * 店铺
     */
    private String store;
    /**
     * 系统权限列表
     */
    private List<String> systemPermissionList;
    /**
     * 订单权限列表
     */
    private List<String> orderPermissionList;
    /**
     * 产品权限列表
     */
    private List<String> productPermissionList;
    /**
     * 亚马逊权限列表
     */
    private List<String> amazonPermissionList;
    /**
     * 转运权限列表
     */
    private List<String> transferPermissionList;
    /**
     * 财务权限列表
     */
    private List<String> moneyPermissionList;
    /**
     * 保底工资
     */
    private BigDecimal minWages;
    /**
     * 工资提成
     */
    private BigDecimal wageStatus;
    /**
     * 销售利润提成
     */
    private BigDecimal profitCommission;
    /**
     * 采购提成
     */
    private BigDecimal purchaseCommission;
    /**
     * 物流提成
     */
    private BigDecimal logisticsCommission;
    /**
     * 开发
     */
    private String develop;
    /**
     * 范围
     */
    private String scope;
    /**
     * 采购
     */
    private String shopping;

}
