package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/16
 * @Time 17:05
 * @Version 1.0
 */
@Data
public class ProductLanguageInfoVo {
    /**
     * 产品编号
     */
    private String productId;
    /**
     * 标题
     */
    private String title;
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 要点
     */
    private String mainPoint;
    /**
     * 描述
     */
    private String describe;
    /**
     * 国家编号
     */
    private String countryId;
}
