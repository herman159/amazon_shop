package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 13:22
 * @Version 1.0
 */
@Data
public class ProductVariantVo {
    /**
     * 变体内容
     */
    private String variant;

    private String us;

    private String ca;

    private String mx;

    private String gb;

    private String fr;

    private String de;

    private String nl;

    private String it;
    /**
     * 替换
     */
    private String productReplace;

}
