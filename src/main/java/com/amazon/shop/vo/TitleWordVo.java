package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 12:35
 * @Version 1.0
 */
@Data
public class TitleWordVo {
    /**
     * 产品编号
     */
    private String productId;
    /**
     * 产品分类
     */
    private String type;
    /**
     * 是否开启
     */
    private String isEnable;
    /**
     * 备注
     */
    private String note;
    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
}
