package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 统计图片Vo类
 *
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 10:20
 * @Version 1.0
 */
@Data
public class ChartData {
    /**
     * 数据来源
     */
    private String source;
    /**
     * 订单数量
     */
    private String orderCount;
    /**
     * 销售额
     */
    private String salesVolume;
    /**
     * 利润
     */
    private String profit;
    /**
     * 日期
     */
    private String date;
}
