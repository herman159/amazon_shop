package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 13:01
 * @Version 1.0
 */
@Data
public class ContentNotesVo {
    /**
     * 备注人员
     */
    private String name;
    /**
     * 备注内容
     */
    private String info;
    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
}
