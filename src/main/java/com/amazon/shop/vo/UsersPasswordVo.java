package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/16
 * @Time 19:27
 * @Version 1.0
 */
@Data
public class UsersPasswordVo {
    /**
     * 旧密码
     */
    private String oldPassword;
    /**
     * 新密码
     */
    private String newPassword;
    /**
     * 确认密码
     */
    private String repeat;
}
