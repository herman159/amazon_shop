package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 12:37
 * @Version 1.0
 */
@Data
public class OrderWayBillVo {
    /**
     * 面单号
     */
    private String wayBillNumber;
    /**
     * 产品
     */
    private CommodityVo commodityVo;
    /**
     * 增加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    /**
     * 转运商姓名
     */
    private String purchaseName;
    /**
     * 转运商
     */
    private String purchaseCenter;
    /**
     * 状态
     */
    private String status;
    /**
     * 称重
     */
    private String widget;
    /**
     * 二次称重
     */
    private String widgetTwo;
    /**
     * 物流
     */
    private String express;
    /**
     * 追踪号
     */
    private String number;
    /**
     * 编号类型
     */
    private String numberType;
    /**
     * 发货日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deliveryDate;
    /**
     * 费用
     */
    private BigDecimal price;
    /**
     * 订单号
     */
    private String aboutOrder;
    /**
     * 销售人员
     */
    private String salesman;
    /**
     * 英文名
     */
    private String enTitle;
    /**
     * 中文名
     */
    private String cnTitle;
    /**
     * 币种
     */
    private String declares;
    /**
     * 申报币种
     */
    private String declareType;
    /**
     * 备注
     */
    private String note;

}
