package com.amazon.shop.vo;

import com.amazon.shop.entity.SellerInfo;
import lombok.Data;

import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 12:28
 * @Version 1.0
 */
@Data
public class OrderSimple {
    /**
     * 订单号
     */
    private String orderNumber;
    /**
     * 亚马逊状态
     */
    private String status;
    /**
     * 订单状态
     */
    private String orderStatus;
    /**
     * 订单子状态
     */
    private String orderChildStatus;
    /**
     * 父异常状态(是否具有异常)
     */
    private String fatherError;
    /**
     * 子异常状态 (异常具体类型)
     */
    private String childError;
    /**
     * 销售员
     */
    private String salesman;
    /**
     * 卖家信息
     */
    private SellerInfo sellerInfo;
    /**
     * 买家信息
     */
    private BuyerInfoVo buyer;
    /**
     * 商品
     */
    private CommodityVo commodityVo;
    /**
     * 采购单
     */
    private OrderPurchaseVo orderPurchaseVo;
    /**
     * 运单号
     */
    private OrderWayBillVo wayBill;
    /**
     * 备注
     */
    private ContentNotesVo note;
    /**
     * 日志
     */
    private List<OperationLogVo> logList;
    /**
     * 添加时间
     */
    private String addTime;
}
