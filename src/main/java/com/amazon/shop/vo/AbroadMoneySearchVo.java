package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/2
 * @Time 19:56
 * @Version 1.0
 */
@Data
public class AbroadMoneySearchVo {
    /**
     * 主键编号
     */
    private String abroadMoneyId;
    /**
     * 录入员
     */
    private String username;
    /**
     * 银行账户
     */
    private String bankAccount;
    /**
     * 公司
     */
    private String company;
    /**
     * 汇入钱数
     */
    private BigDecimal money;
    /**
     * 兑换钱数
     */
    private BigDecimal exchangeMoney;
    /**
     * 备注
     */
    private String note;
    /**
     * 币种
     */
    private String currency;
    /**
     * 汇入国家
     */
    private String country;
    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
}
