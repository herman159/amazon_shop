package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/7/4
 * @Time 12:38
 * @Version 1.0
 */
@Data
public class CodeRate {
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 数据
     */
    private Data data;
    /**
     * 信息
     */
    private String message;

    @lombok.Data
    public static class Data {
        /**
         * 汇率
         */
        private Double rate;
        /**
         * 兑换币种钱数
         */
        private Double toCode;
        /**
         * 被兑换币种钱数
         */
        private Double fromCode;
    }
}
