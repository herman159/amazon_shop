package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/20
 * @Time 12:22
 * @Version 1.0
 */
@Data
public class UsersGetVo {
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;

    /**
     * qq
     */
    private String qq;

    /**
     * 用户token
     */
    private String token;
    /**
     * 头像
     */
    private String avatar;
}
