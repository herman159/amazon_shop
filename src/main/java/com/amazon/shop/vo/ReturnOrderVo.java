package com.amazon.shop.vo;

import com.amazon.shop.entity.ProductPic;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 12:44
 * @Version 1.0
 */
@Data
public class ReturnOrderVo {
    /**
     * 订单号
     */
    private String orderNumber;
    /**
     * 图片
     */
    private List<ProductPicVo> pixMap;
    /**
     * 状态
     */
    private String status;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 协同号
     */
    private String accountNumber;
    /**
     * 存储
     */
    private String storage;
    /**
     * 仓库
     */
    private String warehouse;
    /**
     * 备注
     */
    private String note;
    /**
     * sku
     */
    private String sku;
    /**
     * 增加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;

}
