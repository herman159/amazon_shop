package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 12:04
 * @Version 1.0
 */
@Data
public class SupplierVo {
    /**
     * 姓名
     */
    private String name;
    /**
     * qq
     */
    private String qq;
    /**
     * 电话
     */
    private String phone;
    /**
     * 备注
     */
    private String note;
    /**
     * 联系人
     */
    private String contacts;
    /**
     * 邮编
     */
    private String postCode;
    /**
     * 省份
     */
    private String province;
    /**
     * 城市
     */
    private String city;
    /**
     * 地址
     */
    private String address;
}
