package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 12:57
 * @Version 1.0
 */
@Data
public class OperationLogVo {
    /**
     * 操作业务员编号
     */
    private String salesman;
    /**
     * ip地址
     */
    private String ip;
    /**
     * ip所属地区
     */
    private String area;
    /**
     * 操作类型
     */
    private String type;
    /**
     * 操作明细
     */
    private String detailed;
    /**
     * 操作部门
     */
    private String department;
    /**
     * 数据库表编号
     */
    private String tableId;
}
