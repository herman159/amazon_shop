package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/14
 * @Time 20:53
 * @Version 1.0
 */
@Data
public class ContentNotesSaveVo {
    /**
     * 订单编号
     */
    private String orderId;
    /**
     * 备注人员
     */
    private String name;
    /**
     * 备注内容
     */
    private String info;
}
