package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 15:04
 * @Version 1.0
 */
@Data
public class Wage {
    /**
     * 销售员
     */
    private String salesman;
    /**
     * 保底工资
     */
    private BigDecimal basicSalary;
    /**
     * 销售提成
     */
    private BigDecimal salesCommissions;
    /**
     * 物流提成
     */
    private BigDecimal transferCommission;
    /**
     * 找货提成
     */
    private BigDecimal findCommission;
    /**
     * 应付工资
     */
    private BigDecimal dealSalary;
    /**
     * 实付工资
     */
    private BigDecimal factSalary;
    /**
     * 公司
     */
    private String company;
    /**
     * 年份
     */
    private String year;
    /**
     * 月份
     */
    private String month;

}
