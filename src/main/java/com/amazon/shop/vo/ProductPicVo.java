package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 12:59
 * @Version 1.0
 */
@Data
public class ProductPicVo {
    /**
     * 图片网络地址
     */
    private String picPath;
    /**
     * 图片Oss地址
     */
    private String path;
}
