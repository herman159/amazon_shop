package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/27
 * @Time 17:44
 * @Version 1.0
 */
@Data
public class TortVo {
    /**
     * 侵权词
     */
    private String tortWord;
    /**
     * 备注
     */
    private String remark;
    /**
     * 替换词
     */
    private String replaceWord;
}
