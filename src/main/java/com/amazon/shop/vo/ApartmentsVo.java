package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author 郭非
 * @Date 2021/6/29
 * @Time 16:08
 * @Version 1.0
 */
@Data
public class ApartmentsVo {
    /**
     * 部门名称
     */
    private String apartmentName;
    /**
     * 公司名称
     */
    private String company;
    /**
     * 是否可见
     */
    private String isView;
    /**
     * 是否禁用
     */
    private String isDisable;
    /**
     * 余额
     */
    private BigDecimal balance;
    /**
     * 限额
     */
    private BigDecimal minBalance;
    /**
     * 允许其他部门
     */
    private String isShare;
    /**
     * 允许本部门
     */
    private String isSuperior;

}
