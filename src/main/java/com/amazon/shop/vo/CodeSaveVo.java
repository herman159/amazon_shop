package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/6/24
 * @Time 9:27
 * @Version 1.0
 */
@Data
public class CodeSaveVo {
    /**
     * 类型
     */
    private String type;
    /**
     * 状态
     */
    private String status;
    /**
     * 账户
     */
    private String account;
    /**
     * 产品编号
     */
    private String productId;
    /**
     * 变体
     */
    private String variant;
    /**
     * sku
     */
    private String sku;
    /**
     * 增加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
}
