package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author 郭非
 * @Date 2021/7/2
 * @Time 9:12
 * @Version 1.0
 */
@Data
public class RechargeSearchVo {
    /**
     * 充值提现编号
     */
    private String rechargeId;
    /**
     * 银行名称
     */
    private String bank;
    /**
     * 充值
     */
    private BigDecimal payment;
    /**
     * 姓名
     */
    private String name;
    /**
     * 账户
     */
    private String account;
    /**
     * 备注
     */
    private String remark;
    /**
     * 提现
     */
    private BigDecimal cash;
}
