package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/7/2
 * @Time 9:42
 * @Version 1.0
 */
@Data
public class ApartmentsSearchVo {
    /**
     * 部门编号
     */
    private String apartmentId;
    /**
     * 部门名称
     */
    private String apartmentName;
    /**
     * 公司名称
     */
    private String company;
    /**
     * 是否可见
     */
    private String isView;
    /**
     * 是否禁用
     */
    private String isDisable;
    /**
     * 余额
     */
    private BigDecimal balance;
    /**
     * 限额
     */
    private BigDecimal minBalance;
    /**
     * 允许其他部门
     */
    private String isShare;
    /**
     * 允许本部门
     */
    private String isSuperior;
    /**
     * 部门人员
     */
    private List<String> staffs;
}
