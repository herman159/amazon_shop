package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 13:32
 * @Version 1.0
 */
@Data
public class ProductSimple {
    /**
     * 产品编号
     */
    private String number;
    /**
     * 产品图片
     */
    private List<ProductPicVo> pixmap;
    /**
     * 价格
     */
    private String price;
    /**
     * 利润
     */
    private String profit;
    /**
     * 产品名
     */
    private String name;
    /**
     * 产品类型
     */
    private List<ProductTypeVo> type;
    /**
     * 增加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    /**
     * 销售员
     */
    private String salesman;
}
