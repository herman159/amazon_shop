package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/6/27
 * @Time 19:53
 * @Version 1.0
 */
@Data
public class BankVo {
    /**
     * 公司
     */
    private String company;
    /**
     * 银行名称
     */
    private String bank;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 支付方式
     */
    private String type;
    /**
     * 账户
     */
    private String bankAccount;
    /**
     * 找货
     */
    private String findGoods;
    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
}
