package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 12:35
 * @Version 1.0
 */
@Data
public class ErrorOrder {
    /**
     * 订单号
     */
    private String orderNumber;
    /**
     * 父异常状态(是否具有异常)
     */
    private String fatherError;
    /**
     * 子异常状态 (异常具体类型)
     */
    private String childError;
    /**
     * 销售员
     */
    private String salesman;
    /**
     * 商品
     */
    private CommodityVo commodityVo;
    /**
     * 运单号
     */
    private OrderWayBillVo wayBill;
    /**
     * 备注
     */
    private ContentNotesVo note;
    /**
     * 日志
     */
    private List<OperationLogVo> logList;
    /**
     * 添加时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
}
