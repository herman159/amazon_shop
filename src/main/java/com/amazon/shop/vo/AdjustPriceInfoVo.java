package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 13:45
 * @Version 1.0
 */
@Data
public class AdjustPriceInfoVo {
    /**
     * 主键
     */
    private String adjustPriceId;
    /**
     * 比例
     */
    private String proportion;
    /**
     * 币种
     */
    private BigDecimal currency;
    /**
     * 起始金额
     */
    private BigDecimal startAmount;
    /**
     * 微调
     */
    private BigDecimal minPrice;
    /**
     * 产品一级分类
     */
    private String productTypeOne;
    /**
     * 产品二级分类
     */
    private String productTypeTwo;
    /**
     * 产品三级分类
     */
    private String productTypeThree;
    /**
     * 产品四级分类
     */
    private String productTypeFour;
    /**
     * 产品五级分类
     */
    private String productTypeFive;
    /**
     * 产品六级分类
     */
    private String productTypeSix;

}
