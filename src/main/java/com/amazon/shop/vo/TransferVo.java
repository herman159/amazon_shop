package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 10:00
 * @Version 1.0
 */
@Data
public class TransferVo {
    /**
     * 转入账户
     */
    private String inAccount;
    /**
     * 转出账户
     */
    private String outAccount;
    /**
     * 金额
     */
    private BigDecimal money;
    /**
     * 备注
     */
    private String note;
    /**
     * 录入员
     */
    private String username;
    /**
     * 录入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    /**
     * 日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
}
