package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/29
 * @Time 15:48
 * @Version 1.0
 */
@Data
public class LanguageVo {
    /**
     * 语言
     */
    private String language;
}
