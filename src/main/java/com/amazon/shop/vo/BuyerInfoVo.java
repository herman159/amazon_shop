package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 13:12
 * @Version 1.0
 */
@Data
public class BuyerInfoVo {
    /**
     * 买家姓名
     */
    private String name;
    /**
     * 公司
     */
    private String company;
    /**
     * 地址
     */
    private String address;
    /**
     * 邮编
     */
    private String postCode;
    /**
     * 门牌号
     */
    private String houseNumber;
    /**
     * 城市
     */
    private String city;
    /**
     * 州省
     */
    private String province;
    /**
     * 国家
     */
    private String country;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮箱
     */
    private String mail;
    /**
     * 税号
     */
    private String dutyParagraph;
}
