package com.amazon.shop.vo;

import com.amazon.shop.entity.auth.Permissions;
import lombok.Data;

import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/19
 * @Time 8:25
 * @Version 1.0
 */
@Data
public class RolesSaveVo {
    /**
     * 角色名
     */
    private String rolesName;
    /**
     * 部门
     */
    private String apartment;
    /**
     * 角色权限
     */
    private List<Permissions> permissionsList;

}
