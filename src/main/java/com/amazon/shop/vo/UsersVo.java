package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/16
 * @Time 18:57
 * @Version 1.0
 */
@Data
public class UsersVo {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 用户token
     */
    private String token;
    /**
     * 头像
     */
    private String avatar;
}
