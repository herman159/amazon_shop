package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/23
 * @Time 16:01
 * @Version 1.0
 */
@Data
public class ProductUploadVo {

    private String messageId;

    private String operationType;

    private String sku;

    private String conditionType;

    private String title;

    private String brand;

    private String manufacturer;

    private String mfrPartNumber;

    private String bulletPoint;

    private String description;

    private String type;

    private String value;

    private String recommendedBrowseNode;

    private String variationTheme;

    private String material;

    private String threadCount;

    private String color;

    private String size;
}
