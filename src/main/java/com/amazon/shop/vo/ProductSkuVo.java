package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/7/4
 * @Time 13:33
 * @Version 1.0
 */
@Data
public class ProductSkuVo {
    /**
     * sku编号
     */
    private String skuId;
    /**
     * 产品sku
     */
    private String sku;
    /**
     * 产品编号
     */
    private String productId;
}
