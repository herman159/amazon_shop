package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/7/3
 * @Time 12:20
 * @Version 1.0
 */
@Data
public class CooperativeSearchVo {
    /**
     * 合作伙伴编号
     */
    private String cooperativeId;
    /**
     * 协同号
     */
    private Integer number;
    /**
     * 是否允许分销
     */
    private String isEnableSale;
    /**
     * 比例
     */
    private String proportion;
    /**
     * 姓名
     */
    private String name;
    /**
     * 公司
     */
    private String company;
    /**
     * qq
     */
    private String qq;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮箱
     */
    private String mail;
    /**
     * 备注
     */
    private String note;
}
