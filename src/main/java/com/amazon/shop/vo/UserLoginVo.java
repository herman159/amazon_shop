package com.amazon.shop.vo;

import lombok.Data;

/**
 * @Author 郭非
 * @Date 2021/6/24
 * @Time 15:34
 * @Version 1.0
 */
@Data
public class UserLoginVo {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
}
