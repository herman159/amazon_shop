package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author 郭非
 * @Date 2021/6/27
 * @Time 20:53
 * @Version 1.0
 */
@Data
public class WithdrawalVo {
    /**
     * 银行名称
     */
    private String bank;
    /**
     * 提现
     */
    private BigDecimal withdrawal;
    /**
     * 姓名
     */
    private String name;
    /**
     * 账户
     */
    private String account;
    /**
     * 备注
     */
    private String remark;
}
