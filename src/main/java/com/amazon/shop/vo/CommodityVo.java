package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
public class CommodityVo {
    /**
     * 产品编号
     */
    private String number;
    /**
     * 产品标题
     */
    private String title;
    /**
     * 产品变体
     */
    private String variant;
    /**
     * 产品价格
     */
    private BigDecimal price;
    /**
     * 成本
     */
    private BigDecimal basicPrice;
    /**
     * 利润
     */
    private BigDecimal profit;
    /**
     * 币种
     */
    private String currency;
    /**
     * 产品数量
     */
    private Integer count;
    /**
     * 产品图片
     */
    private List<ProductPicVo> pixMap;
    /**
     * 产品sku
     */
    private String sku;

}
