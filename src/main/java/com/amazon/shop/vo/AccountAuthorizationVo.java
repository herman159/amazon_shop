package com.amazon.shop.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 20:07
 * @Version 1.0
 */
@Data
public class AccountAuthorizationVo {
    /**
     * 站点编号
     */
    private String number;
    /**
     * 授权账户
     */
    private String authorizationAccount;
    /**
     * 启用
     */
    private String enable;
    /**
     * 站点名
     */
    private String shopName;
    /**
     * 编码类型
     */
    private String codeType;
    /**
     * 独立编码
     */
    private String independentCode;
    /**
     * 备货天数
     */
    private String dayNumber;
    /**
     * 基准账户
     */
    private String benchmarkAccount;
    /**
     * fba是否开启
     */
    private String fbaEnable;
    /**
     * 百分比
     */
    private String percentage;
    /**
     * 业务员
     */
    private String salesman;
    /**
     * 资料归属
     */
    private String belonger;
    /**
     * 登录帐号
     */
    private String loginId;
    /**
     * 信用卡号
     */
    private String creditCardNumber;
    /**
     * 国家编号
     */
    private String countryId;
    /**
     * 开发商名称
     */
    private String manufacturerNumber;
    /**
     * 卖家编号
     */
    private String sellerNumber;
    /**
     * 授权令牌
     */
    private String authorizationToken;
    /**
     * 货币种类
     */
    private String currencyType;
    /**
     * 最小价格
     */
    private BigDecimal minimumSellingPrice;
    /**
     * 最大价格
     */
    private BigDecimal maximumSellingPrice;
    /**
     * 调整
     */
    private String adjust;
    /**
     * 执照法人
     */
    private String legalPerson;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * p卡或wf
     */
    private String pCardOrWfCard;
    /**
     * vps地址
     */
    private String vpsAddress;
    /**
     * vps用户
     */
    private String vpsUser;
    /**
     * vps密码
     */
    private String vpsPassword;
    /**
     * 最后提交时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date lastSubmitTime;
}
