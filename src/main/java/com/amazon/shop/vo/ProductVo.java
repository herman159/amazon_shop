package com.amazon.shop.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author 郭非
 * @Date 2021/6/14
 * @Time 18:19
 * @Version 1.0
 */
@Data
public class ProductVo {
    /**
     * 产品编号
     */
    private String numbers;
    /**
     * 产品类型
     */
    private ProductTypeVo productTypeVo;
    /**
     * 审核状态
     */
    private String status;
    /**
     * 产品标题
     */
    private String title;
    /**
     * 产品图片
     */
    private List<ProductPicVo> images;
    /**
     * 产品Sku
     */
    private String sku;
    /**
     * 上架下架
     */
    private String upDown;
    /**
     * 安全等级
     */
    private String safeLevel;
    /**
     * 产品等级
     */
    private String productLevel;
    /**
     * 产品开发
     */
    private String development;
    /**
     * 美工修图
     */
    private String beautifyPic;
    /**
     * 产品品牌
     */
    private String brand;
    /**
     * 厂商名
     */
    private String manuName;
    /**
     * 厂商编号
     */
    private String manuNum;
    /**
     * 原产地
     */
    private String origin;
    /**
     * 产品目录
     */
    private String catalog;
    /**
     * 海关编号
     */
    private Integer customsNum;
    /**
     * 中文简称
     */
    private String znName;
    /**
     * 英文简称
     */
    private String cnName;
    /**
     * 申报单价
     */
    private String declare;
    /**
     * 包装毛重
     */
    private String weigh;
    /**
     * 长
     */
    private Double productL;
    /**
     * 高
     */
    private String productH;
    /**
     * 宽
     */
    private String productW;
    /**
     * 特殊类型
     */
    private String specialType;
    /**
     * 产品币种
     */
    private String currency;
    /**
     * 成本单价
     */
    private BigDecimal costPrice;
    /**
     * 固定运费
     */
    private String freight;
    /**
     * 国家运费
     */
    private String countryFreight;
    /**
     * 商品视频
     */
    private String videoWeb;
    /**
     * 供应商名
     */
    private String supplierName;
    /**
     * 供应货号
     */
    private String supplierNum;
    /**
     * 采集网站
     */
    private String collectionWeb;
    /**
     * 产品备注
     */
    private String note;
    /**
     * 适合人群
     */
    private String fitPeople;
    /**
     * 年龄分组
     */
    private String fitAge;
    /**
     * 产品材料
     */
    private String material;
    /**
     * 外部材料
     */
    private String outMaterial;
    /**
     * 金属类型
     */
    private String metal;
    /**
     * 珠宝类型
     */
    private String jewelry;
    /**
     * 鞋类宽度
     */
    private String shoeSize;
    /**
     * 变体
     */
    private List<ProductVariantVo> variantMap;
    /**
     * 操作日志
     */
    private List<OperationLogVo> logs;
    /**
     * 产品翻译信息
     */
    private ProductLanguageInfoVo productLanguageInfoVo;
}
