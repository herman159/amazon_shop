package com.amazon.shop.exception;

import com.amazon.shop.utils.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 自定义异常处理类
 *
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 9:23
 * @Version 1.0
 */
@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {
    /**
     * 自定义失败处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = CustomException.class)
    public CommonResult customExceptionHandler(CustomException e) {
        log.info(e.getCode() + e.getMessage());
        return CommonResult.error(e.getCode(), e.getMessage());
    }

    /**
     * 通用失败处理方法
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public CommonResult GlobalExceptionHandler(Exception e) {
        log.info(e.getMessage());
        return CommonResult.error(e.getMessage());
    }
}
