package com.amazon.shop.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 异常信息类
 *
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 9:22
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomException extends RuntimeException {
    /**
     * 错误状态码
     */
    private Integer code;
    /**
     * 错误信息
     */
    private String message;
}
