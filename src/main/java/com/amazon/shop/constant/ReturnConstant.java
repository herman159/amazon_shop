package com.amazon.shop.constant;

/**
 * 返回信息常量接口
 *
 * @Author 郭非
 * @Date 2021/6/12
 * @Time 9:16
 * @Version 1.0
 */
public interface ReturnConstant {
    /**
     * 成功信息
     */
    String HTTP_RES_CODE_200_VALUE = "请求成功！";
    /**
     * 错误信息
     */
    String HTTP_RES_CODE_500_VALUE = "系统错误";
    /**
     * 成功状态码
     */
    Integer HTTP_RES_CODE_200 = 200;
    /**
     * 错误状态码
     */
    Integer HTTP_RES_CODE_500 = 500;
    /**
     * 系统异常状态码
     */
    Integer SYSTEM_ERROR = 400;
    /**
     * 数据上传数量限制
     */
    Integer DATA_SIZE_LIMIT = 500;
    /**
     * 拉取订单失败状态码
     */
    Integer PULL_ORDERS_ERROR_CODE = 401;
    /**
     * 拉取订单失败信息
     */
    String PULL_ORDERS_ERROR_MESSAGE = "拉取订单失败";
    /**
     * 日志保存失败状态码
     */
    Integer SAVE_LOG_ERROR_CODE = 402;
    /**
     * 日志保存失败信息
     */
    String SAVE_LOG_ERROR_MESSAGE = "日志保存失败";
    /**
     * 用户登录失败状态码
     */
    Integer USER_LOGIN_ERROR_CODE = 403;
    /**
     * 用户登录失败信息
     */
    String USER_LOGIN_ERROR_MESSAGE = "用户名或密码错误";
    /**
     * 修改密码失败状态码
     */
    Integer UPDATE_PASSWORD_ERROR_CODE = 404;
    /**
     * 修改密码失败信息
     */
    String UPDATE_PASSWORD_ERROR_MESSAGE = "修改密码失败";
    /**
     * 生成XML文件失败状态码
     */
    Integer CREATE_XML_ERROR_CODE = 405;
    /**
     * 生成XML文件失败信息
     */
    String CREATE_XML_ERROR_MESSAGE = "生成XML文件失败";
    /**
     * 上传产品失败状态码
     */
    Integer UPLOAD_PRODUCT_ERROR_CODE = 406;
    /**
     * 上传产品失败信息
     */
    String UPLOAD_PRODUCT_ERROR_MESSAGE = "上传产品失败";
    /**
     * 更新用户信息失败状态码
     */
    Integer UPDATE_USER_INFO_ERROR_CODE = 407;
    /**
     * 更新用户信息失败信息
     */
    String UPDATE_USER_INFO_ERROR_MESSAGE = "更新用户信息失败";
    /**
     * 日志无编号错误码
     */
    Integer NO_LOG_NUMBER_ERROR_CODE = 408;
    /**
     * 日志无编号错误信息
     */
    String NO_LOG_NUMBER_ERROR_MESSAGE = "日志无编号";
    /**
     * 无权限访问状态码
     */
    Integer NO_PERMISSION_CODE = 409;
    /**
     * 无权限访问信息
     */
    String NO_PERMISSION_MESSAGE = "无权限访问";
    /**
     * 获取异常订单错误状态码
     */
    Integer GET_ERROR_LIST_ERROR_CODE = 410;
    /**
     * 获取异常订单错误信息
     */
    String GET_ERROR_LIST_ERROR_MESSAGE = "获取异常订单失败";
    /**
     * 时间格式错误状态码
     */
    Integer DATA_FORMAT_ERROR_code = 411;
    /**
     * 格式错误信息
     */
    String DATE_FORMAT_ERROR_MESSAGE = "时间格式有误";
    /**
     * 上传类型
     */
    String POST_PRODUCT_DATA = "POST_PRODUCT_DATA";
    /**
     * 余额不足错误状态码
     */
    Integer BALANCE_NOT_ENOUGH_CODE = 412;
    /**
     * 余额不足错误信息
     */
    String BALANCE_NOT_ENOUGH_MESSAGE = "余额不足";
    /**
     * 无账户不能提现错误码
     */
    Integer NO_ACCOUNT_ERROR_CODE = 413;
    /**
     * 无账户不能提现错误信息
     */
    String NO_ACCOUNT_ERROR_MESSAGE = "无账户不能提现";
    /**
     * Token重复错误码
     */
    Integer HAD_TOKEN_ERROR_CODE = 414;
    /**
     * Token重复错误信息
     */
    String HAD_TOKEN_ERROR_MESSAGE = "Token已存在";
}
