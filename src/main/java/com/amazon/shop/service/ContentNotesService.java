package com.amazon.shop.service;

import com.amazon.shop.entity.ContentNotes;
import com.amazon.shop.vo.ContentNotesSaveVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
public interface ContentNotesService extends IService<ContentNotes> {
    /**
     * 添加备注
     *
     * @param contentNotesSaveVo
     * @param token
     */
    void saveContentNotes(ContentNotesSaveVo contentNotesSaveVo, String token);
}
