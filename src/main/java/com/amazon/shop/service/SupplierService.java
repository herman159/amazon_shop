package com.amazon.shop.service;

import com.amazon.shop.entity.Supplier;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
public interface SupplierService extends IService<Supplier> {

}
