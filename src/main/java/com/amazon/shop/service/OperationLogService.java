package com.amazon.shop.service;

import com.amazon.shop.entity.OperationLog;
import com.baomidou.mybatisplus.extension.service.IService;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
public interface OperationLogService extends IService<OperationLog> {
    /**
     * 保存日志(String类型)
     *
     * @param joinPoint
     */
    void saveOperationLog(ProceedingJoinPoint joinPoint, String id, String salesman);

    /**
     * 批量保存日志
     *
     * @param joinPoint
     * @param idList
     */
    void saveOperationLogBatch(ProceedingJoinPoint joinPoint, List<String> idList, String salesman);

}
