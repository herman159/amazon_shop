package com.amazon.shop.service;

import com.amazon.shop.entity.OrderWayBill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
public interface OrderWayBillService extends IService<OrderWayBill> {

}
