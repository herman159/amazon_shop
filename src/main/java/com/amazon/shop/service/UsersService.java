package com.amazon.shop.service;

import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.vo.UserLoginVo;
import com.amazon.shop.vo.UsersPasswordVo;
import com.amazon.shop.vo.UsersVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
public interface UsersService extends IService<Users> {
    /**
     * 用户登录
     *
     * @param userLoginVo
     * @return
     */
    UsersVo login(UserLoginVo userLoginVo);

    /**
     * 修改密码
     *
     * @param passwordVo
     */
    void updatePassword(UsersPasswordVo passwordVo,String token);
}
