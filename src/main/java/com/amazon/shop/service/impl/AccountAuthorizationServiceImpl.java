package com.amazon.shop.service.impl;

import com.amazon.shop.entity.AccountAuthorization;
import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.entity.Country;
import com.amazon.shop.entity.Salesman;
import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.mapper.AccountAuthorizationMapper;
import com.amazon.shop.mapper.CountryMapper;
import com.amazon.shop.service.AccountAuthorizationService;
import com.amazon.shop.service.AmazonAuthorityService;
import com.amazon.shop.service.SalesmanService;
import com.amazon.shop.utils.NumberUtils;
import com.amazon.shop.vo.AccountAuthorizationSaveVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class AccountAuthorizationServiceImpl extends ServiceImpl<AccountAuthorizationMapper, AccountAuthorization> implements AccountAuthorizationService {
    @Resource
    private CountryMapper countryMapper;
    @Resource
    private AmazonAuthorityService amazonAuthorityService;
    @Resource
    private SalesmanService salesmanService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private NumberUtils numberUtils;

    /**
     * 保存账户授权信息
     *
     * @param authorizationVo
     */
    @Override
    @Transactional
    public String saveAccountAuthorization(AccountAuthorizationSaveVo authorizationVo) {
        AccountAuthorization accountAuthorization = new AccountAuthorization();
        //属性对拷
        BeanUtils.copyProperties(authorizationVo, accountAuthorization);
        //自定义亚马逊授权店铺编号
        String randomNumber = numberUtils.getSixRandomNumber();
        String id = randomNumber + "amazonAccount";
        accountAuthorization.setNumber(id);

        //通过销售员编号查询销售员
        String salesmanName = authorizationVo.getSalesman();
        String belong = authorizationVo.getBelong();
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("real_name", salesmanName);
        Users users = usersService.getOne(usersQueryWrapper);
        //设置销售员编号
        String userId = users.getUserId();
        accountAuthorization.setSalesmanId(userId);
        //保存销售员信息
        Salesman salesman = new Salesman();
        salesman.setWebsiteNumber(id);
        salesman.setSalesmanName(salesmanName);
        salesman.setSalesmanId(userId);
        salesmanService.save(salesman);

        //设置资料归属
        QueryWrapper<Salesman> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("salesman_name", belong);
        Salesman salesmanServiceTwo = salesmanService.getOne(queryWrapper);
        String salesmanIdTwo = salesmanServiceTwo.getSalesmanId();
        accountAuthorization.setBelongId(salesmanIdTwo);

        //设置时间
        Date lastSubmitTime = authorizationVo.getLastSubmitTime();
        accountAuthorization.setGmtCreated(lastSubmitTime.toString());

        //通过国家编号查询国家信息
        String countryId = authorizationVo.getCountryId();
        Country country = countryMapper.selectById(countryId);
        String name = country.getName();
        String group = country.getCountryGroup();
        if (!StringUtils.isEmpty(name)) {
            accountAuthorization.setCountryName(name);
        }
        if (!StringUtils.isEmpty(group)) {
            accountAuthorization.setCountryGroup(group);
        }
        //保存亚马逊授权信息
        boolean save = this.save(accountAuthorization);

        //将授权信息保存到开发者授权信息表中
        if (save) {
            AmazonAuthority amazonAuthority = new AmazonAuthority();
            //设置站点编号
            amazonAuthority.setId(id);
            amazonAuthority.setRefreshToken(accountAuthorization.getAuthorizationToken());
            amazonAuthority.setSpEndPoint(country.getSpEndpoint());
            amazonAuthority.setRegion(country.getAws());
            amazonAuthorityService.save(amazonAuthority);
        }
        return id;
    }
}
