package com.amazon.shop.service.impl;

import com.amazon.shop.entity.ProductVariant;
import com.amazon.shop.mapper.ProductVariantMapper;
import com.amazon.shop.service.ProductVariantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class ProductVariantServiceImpl extends ServiceImpl<ProductVariantMapper, ProductVariant> implements ProductVariantService {

}
