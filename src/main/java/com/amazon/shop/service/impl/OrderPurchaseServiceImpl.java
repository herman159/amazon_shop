package com.amazon.shop.service.impl;

import com.amazon.shop.entity.OrderPurchase;
import com.amazon.shop.mapper.OrderPurchaseMapper;
import com.amazon.shop.service.OrderPurchaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class OrderPurchaseServiceImpl extends ServiceImpl<OrderPurchaseMapper, OrderPurchase> implements OrderPurchaseService {

}
