package com.amazon.shop.service.impl;

import com.amazon.shop.entity.AdjustPriceInfo;
import com.amazon.shop.mapper.AdjustPriceInfoMapper;
import com.amazon.shop.service.AdjustPriceInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class AdjustPriceInfoServiceImpl extends ServiceImpl<AdjustPriceInfoMapper, AdjustPriceInfo> implements AdjustPriceInfoService {

}
