package com.amazon.shop.service.impl;

import com.amazon.shop.entity.FinanceBalance;
import com.amazon.shop.mapper.FinanceBalanceMapper;
import com.amazon.shop.service.FinanceBalanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Service
public class FinanceBalanceServiceImpl extends ServiceImpl<FinanceBalanceMapper, FinanceBalance> implements FinanceBalanceService {

}
