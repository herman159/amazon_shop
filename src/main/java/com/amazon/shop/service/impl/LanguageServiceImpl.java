package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Language;
import com.amazon.shop.mapper.LanguageMapper;
import com.amazon.shop.service.LanguageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
@Service
public class LanguageServiceImpl extends ServiceImpl<LanguageMapper, Language> implements LanguageService {

}
