package com.amazon.shop.service.impl;

import com.amazon.shop.entity.BuyerInfo;
import com.amazon.shop.mapper.BuyerInfoMapper;
import com.amazon.shop.service.BuyerInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class BuyerInfoServiceImpl extends ServiceImpl<BuyerInfoMapper, BuyerInfo> implements BuyerInfoService {

}
