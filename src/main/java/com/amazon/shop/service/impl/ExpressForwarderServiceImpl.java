package com.amazon.shop.service.impl;

import com.amazon.shop.entity.ExpressForwarder;
import com.amazon.shop.mapper.ExpressForwarderMapper;
import com.amazon.shop.service.ExpressForwarderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class ExpressForwarderServiceImpl extends ServiceImpl<ExpressForwarderMapper, ExpressForwarder> implements ExpressForwarderService {

}
