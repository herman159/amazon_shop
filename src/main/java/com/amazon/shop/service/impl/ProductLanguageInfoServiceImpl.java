package com.amazon.shop.service.impl;

import com.amazon.shop.entity.ProductLanguageInfo;
import com.amazon.shop.mapper.ProductLanguageInfoMapper;
import com.amazon.shop.service.ProductLanguageInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-16
 */
@Service
public class ProductLanguageInfoServiceImpl extends ServiceImpl<ProductLanguageInfoMapper, ProductLanguageInfo> implements ProductLanguageInfoService {

}
