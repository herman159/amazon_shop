package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Tort;
import com.amazon.shop.mapper.TortMapper;
import com.amazon.shop.service.TortService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Service
public class TortServiceImpl extends ServiceImpl<TortMapper, Tort> implements TortService {

}
