package com.amazon.shop.service.impl;

import com.amazon.shop.entity.MessageStatus;
import com.amazon.shop.mapper.MessageStatusMapper;
import com.amazon.shop.service.MessageStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-05
 */
@Service
public class MessageStatusServiceImpl extends ServiceImpl<MessageStatusMapper, MessageStatus> implements MessageStatusService {

}
