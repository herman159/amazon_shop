package com.amazon.shop.service.impl;

import com.amazon.shop.entity.UploadData;
import com.amazon.shop.mapper.UploadDataMapper;
import com.amazon.shop.service.UploadDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-05
 */
@Service
public class UploadDataServiceImpl extends ServiceImpl<UploadDataMapper, UploadData> implements UploadDataService {

}
