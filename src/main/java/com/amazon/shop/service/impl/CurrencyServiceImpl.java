package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Currency;
import com.amazon.shop.mapper.CurrencyMapper;
import com.amazon.shop.service.CurrencyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-15
 */
@Service
public class CurrencyServiceImpl extends ServiceImpl<CurrencyMapper, Currency> implements CurrencyService {

}
