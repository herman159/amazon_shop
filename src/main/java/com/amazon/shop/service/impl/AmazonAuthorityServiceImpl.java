package com.amazon.shop.service.impl;

import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.mapper.AmazonAuthorityMapper;
import com.amazon.shop.service.AmazonAuthorityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class AmazonAuthorityServiceImpl extends ServiceImpl<AmazonAuthorityMapper, AmazonAuthority> implements AmazonAuthorityService {

}
