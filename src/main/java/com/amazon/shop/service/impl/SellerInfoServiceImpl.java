package com.amazon.shop.service.impl;

import com.amazon.shop.entity.SellerInfo;
import com.amazon.shop.mapper.SellerInfoMapper;
import com.amazon.shop.service.SellerInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class SellerInfoServiceImpl extends ServiceImpl<SellerInfoMapper, SellerInfo> implements SellerInfoService {

}
