package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Company;
import com.amazon.shop.mapper.CompanyMapper;
import com.amazon.shop.service.CompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements CompanyService {

}
