package com.amazon.shop.service.impl;

import com.amazon.shop.entity.OrderWayBill;
import com.amazon.shop.mapper.OrderWayBillMapper;
import com.amazon.shop.service.OrderWayBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class OrderWayBillServiceImpl extends ServiceImpl<OrderWayBillMapper, OrderWayBill> implements OrderWayBillService {

}
