package com.amazon.shop.service.impl;

import com.amazon.shop.entity.auth.RolesPermissions;
import com.amazon.shop.mapper.RolesPermissionsMapper;
import com.amazon.shop.service.RolesPermissionsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@Service
public class RolesPermissionsServiceImpl extends ServiceImpl<RolesPermissionsMapper, RolesPermissions> implements RolesPermissionsService {

}
