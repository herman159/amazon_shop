package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Salesman;
import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.mapper.SalesmanMapper;
import com.amazon.shop.mapper.UsersMapper;
import com.amazon.shop.service.SalesmanService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-18
 */
@Service
public class SalesmanServiceImpl extends ServiceImpl<SalesmanMapper, Salesman> implements SalesmanService {
    @Resource
    private SalesmanMapper salesmanMapper;
    @Resource
    private UsersMapper usersMapper;

    /**
     * 查询业务员信息
     *
     * @param token
     * @return
     */
    public String getSalesman(String token) {

        //通过token查询用户信息
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("token", token);
        Users users = usersMapper.selectOne(usersQueryWrapper);

        //通过用户编号查询业务员信息
        String userId = users.getUserId();
        QueryWrapper<Salesman> salesmanQueryWrapper = new QueryWrapper<>();
        salesmanQueryWrapper.eq("salesman_id", userId);
        Salesman salesman = salesmanMapper.selectOne(salesmanQueryWrapper);
        String salesmanName = salesman.getSalesmanName();

        return salesmanName;
    }
}
