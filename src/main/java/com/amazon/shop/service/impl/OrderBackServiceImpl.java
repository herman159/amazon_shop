package com.amazon.shop.service.impl;

import com.amazon.shop.entity.OrderBack;
import com.amazon.shop.mapper.OrderBackMapper;
import com.amazon.shop.service.OrderBackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-04
 */
@Service
public class OrderBackServiceImpl extends ServiceImpl<OrderBackMapper, OrderBack> implements OrderBackService {

}
