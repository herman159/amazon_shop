package com.amazon.shop.service.impl;

import com.amazon.shop.entity.ReturnOrder;
import com.amazon.shop.mapper.ReturnOrderMapper;
import com.amazon.shop.service.ReturnOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class ReturnOrderServiceImpl extends ServiceImpl<ReturnOrderMapper, ReturnOrder> implements ReturnOrderService {

}
