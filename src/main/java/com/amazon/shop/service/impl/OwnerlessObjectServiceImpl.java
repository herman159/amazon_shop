package com.amazon.shop.service.impl;

import com.amazon.shop.entity.OwnerlessObject;
import com.amazon.shop.mapper.OwnerlessObjectMapper;
import com.amazon.shop.service.OwnerlessObjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-21
 */
@Service
public class OwnerlessObjectServiceImpl extends ServiceImpl<OwnerlessObjectMapper, OwnerlessObject> implements OwnerlessObjectService {

}
