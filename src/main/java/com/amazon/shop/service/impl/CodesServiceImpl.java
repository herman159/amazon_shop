package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Codes;
import com.amazon.shop.mapper.CodesMapper;
import com.amazon.shop.service.CodesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-24
 */
@Service
public class CodesServiceImpl extends ServiceImpl<CodesMapper, Codes> implements CodesService {

}
