package com.amazon.shop.service.impl;

import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.mapper.UsersMapper;
import com.amazon.shop.service.UsersService;
import com.amazon.shop.utils.NumberUtils;
import com.amazon.shop.vo.UserLoginVo;
import com.amazon.shop.vo.UsersPasswordVo;
import com.amazon.shop.vo.UsersVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {
    @Resource
    private UsersMapper usersMapper;
    @Resource
    private NumberUtils numberUtils;

    /**
     * 用户登录
     *
     * @param userLoginVo
     * @return
     */
    @Override
    public UsersVo login(UserLoginVo userLoginVo) {
        String userName = userLoginVo.getUserName();
        String password = userLoginVo.getPassword();
        //判断用户名密码同时是否正确
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("user_name", userName);

        Users one = usersMapper.selectOne(usersQueryWrapper);
        //判断正确
        if (one != null) {
            String passwordDataBase = one.getPassword();
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            boolean matches = encoder.matches(password, passwordDataBase);
            if (matches) {
                //设置返回用户数据
                UsersVo usersVoReturn = new UsersVo();
                BeanUtils.copyProperties(one, usersVoReturn);
                //返回密码设置为空
                usersVoReturn.setPassword("");
                //生成随机十六位token
                String token = numberUtils.getSixteenRandomNumber();
                usersVoReturn.setToken(token);
                //保存token信息
                one.setToken(token);
                usersMapper.updateById(one);
                return usersVoReturn;
            } else {
                throw new CustomException(ReturnConstant.USER_LOGIN_ERROR_CODE
                        , ReturnConstant.USER_LOGIN_ERROR_MESSAGE);
                //判断错误
            }
        } else {
            throw new CustomException(ReturnConstant.USER_LOGIN_ERROR_CODE
                    , ReturnConstant.USER_LOGIN_ERROR_MESSAGE);
        }
    }

    /**
     * 修改密码
     *
     * @param passwordVo
     * @param token
     */
    @Override
    public void updatePassword(UsersPasswordVo passwordVo, String token) {
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("token", token);
        Users users = usersMapper.selectOne(usersQueryWrapper);
        //获取输入的旧密码
        String oldPassword = passwordVo.getOldPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        //获取输入的新密码
        String newPassword = passwordVo.getNewPassword();
        //获取输入的确认密码
        String repeat = passwordVo.getRepeat();
        if (users != null) {
            String password = users.getPassword();
            if (encoder.matches(oldPassword, password)
                    && newPassword.equals(repeat)
                    && !encoder.matches(newPassword, password)) {
                //设置加密后的密码
                String encode = encoder.encode(newPassword);
                users.setPassword(encode);
                usersMapper.updateById(users);
            }
        } else {
            throw new CustomException(ReturnConstant.UPDATE_PASSWORD_ERROR_CODE
                    , ReturnConstant.UPDATE_PASSWORD_ERROR_MESSAGE);
        }
    }

    /**
     * 获取用户真实姓名
     *
     * @param token
     * @return
     */
    public String getUserRealName(String token) {
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("token", token);
        Users users = usersMapper.selectOne(usersQueryWrapper);
        String realName = users.getRealName();
        return realName;
    }
}
