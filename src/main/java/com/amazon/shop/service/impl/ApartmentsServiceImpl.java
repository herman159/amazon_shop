package com.amazon.shop.service.impl;

import com.amazon.shop.entity.auth.Apartments;
import com.amazon.shop.mapper.ApartmentsMapper;
import com.amazon.shop.service.ApartmentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
@Service
public class ApartmentsServiceImpl extends ServiceImpl<ApartmentsMapper, Apartments> implements ApartmentsService {

}
