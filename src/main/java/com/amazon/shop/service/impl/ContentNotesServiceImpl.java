package com.amazon.shop.service.impl;

import com.amazon.shop.entity.ContentNotes;
import com.amazon.shop.entity.Orders;
import com.amazon.shop.mapper.ContentNotesMapper;
import com.amazon.shop.mapper.OrderSimpleMapper;
import com.amazon.shop.service.ContentNotesService;
import com.amazon.shop.vo.ContentNotesSaveVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class ContentNotesServiceImpl extends ServiceImpl<ContentNotesMapper, ContentNotes> implements ContentNotesService {
    @Resource
    private OrderSimpleMapper orderSimpleMapper;

    @Override
    @Transactional
    public void saveContentNotes(ContentNotesSaveVo contentNotesSaveVo, String token) {
        ContentNotes contentNotes = new ContentNotes();
        //属性对拷
        BeanUtils.copyProperties(contentNotesSaveVo, contentNotes);

        //通过订单编号查询订单
        String orderId = contentNotesSaveVo.getOrderId();
        Orders orders = orderSimpleMapper.selectById(orderId);
        String info = contentNotes.getInfo();

        //将备注信息保存到订单
        orders.setContentNotes(info);
        orderSimpleMapper.updateById(orders);

        //保存备注信息
        this.save(contentNotes);
    }
}
