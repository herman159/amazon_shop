package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Recharge;
import com.amazon.shop.mapper.RechargeMapper;
import com.amazon.shop.service.RechargeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Service
public class RechargeServiceImpl extends ServiceImpl<RechargeMapper, Recharge> implements RechargeService {

}
