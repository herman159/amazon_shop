package com.amazon.shop.service.impl;

import com.amazon.shop.entity.auth.UsersRoles;
import com.amazon.shop.mapper.UsersRolesMapper;
import com.amazon.shop.service.UsersRolesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@Service
public class UsersRolesServiceImpl extends ServiceImpl<UsersRolesMapper, UsersRoles> implements UsersRolesService {

}
