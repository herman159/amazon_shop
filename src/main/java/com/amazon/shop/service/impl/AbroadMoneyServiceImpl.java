package com.amazon.shop.service.impl;

import com.amazon.shop.entity.AbroadMoney;
import com.amazon.shop.mapper.AbroadMoneyMapper;
import com.amazon.shop.service.AbroadMoneyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
@Service
public class AbroadMoneyServiceImpl extends ServiceImpl<AbroadMoneyMapper, AbroadMoney> implements AbroadMoneyService {

}
