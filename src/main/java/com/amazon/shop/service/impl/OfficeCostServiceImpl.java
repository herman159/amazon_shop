package com.amazon.shop.service.impl;

import com.amazon.shop.entity.OfficeCost;
import com.amazon.shop.mapper.OfficeCostMapper;
import com.amazon.shop.service.OfficeCostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
@Service
public class OfficeCostServiceImpl extends ServiceImpl<OfficeCostMapper, OfficeCost> implements OfficeCostService {

}
