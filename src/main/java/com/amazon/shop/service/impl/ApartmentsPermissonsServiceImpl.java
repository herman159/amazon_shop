package com.amazon.shop.service.impl;

import com.amazon.shop.entity.auth.ApartmentsPermissions;
import com.amazon.shop.mapper.ApartmentsPermissonsMapper;
import com.amazon.shop.service.ApartmentsPermissonsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@Service
public class ApartmentsPermissonsServiceImpl extends ServiceImpl<ApartmentsPermissonsMapper, ApartmentsPermissions> implements ApartmentsPermissonsService {

}
