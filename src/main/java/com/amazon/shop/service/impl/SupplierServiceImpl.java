package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Supplier;
import com.amazon.shop.mapper.SupplierMapper;
import com.amazon.shop.service.SupplierService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@Service
public class SupplierServiceImpl extends ServiceImpl<SupplierMapper, Supplier> implements SupplierService {

}
