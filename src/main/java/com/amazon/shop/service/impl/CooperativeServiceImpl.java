package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Cooperative;
import com.amazon.shop.mapper.CooperativeMapper;
import com.amazon.shop.service.CooperativeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@Service
public class CooperativeServiceImpl extends ServiceImpl<CooperativeMapper, Cooperative> implements CooperativeService {

}
