package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Orders;
import com.amazon.shop.mapper.OrderSimpleMapper;
import com.amazon.shop.service.OrderSimpleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Service
public class OrderSimpleServiceImpl extends ServiceImpl<OrderSimpleMapper, Orders> implements OrderSimpleService {

}
