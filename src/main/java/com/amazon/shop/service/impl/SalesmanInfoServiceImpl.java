package com.amazon.shop.service.impl;

import com.amazon.shop.entity.SalesmanInfo;
import com.amazon.shop.mapper.SalesmanInfoMapper;
import com.amazon.shop.service.SalesmanInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-07-01
 */
@Service
public class SalesmanInfoServiceImpl extends ServiceImpl<SalesmanInfoMapper, SalesmanInfo> implements SalesmanInfoService {

}
