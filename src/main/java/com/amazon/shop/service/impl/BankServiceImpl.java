package com.amazon.shop.service.impl;

import com.amazon.shop.entity.Bank;
import com.amazon.shop.mapper.BankMapper;
import com.amazon.shop.service.BankService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Service
public class BankServiceImpl extends ServiceImpl<BankMapper, Bank> implements BankService {

}
