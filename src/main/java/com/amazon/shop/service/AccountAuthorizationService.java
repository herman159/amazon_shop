package com.amazon.shop.service;

import com.amazon.shop.entity.AccountAuthorization;
import com.amazon.shop.vo.AccountAuthorizationSaveVo;
import com.amazon.shop.vo.AccountAuthorizationVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
public interface AccountAuthorizationService extends IService<AccountAuthorization> {
    /**
     * 保存账户授权信息
     *
     * @param authorizationVo
     */
    String saveAccountAuthorization(AccountAuthorizationSaveVo authorizationVo);
}
