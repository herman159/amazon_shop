package com.amazon.shop.service;

import com.amazon.shop.entity.Commodity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
public interface CommodityService extends IService<Commodity> {

}
