package com.amazon.shop.api;

import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.spapi.SellingPartnerAPIAA.AWSAuthenticationCredentials;
import com.amazon.shop.spapi.SellingPartnerAPIAA.AWSAuthenticationCredentialsProvider;
import com.amazon.shop.spapi.SellingPartnerAPIAA.LWAAuthorizationCredentials;
import com.amazon.shop.spapi.api.UploadsApi;

/**
 * @Author 郭非
 * @Date 2021/6/17
 * @Time 16:51
 * @Version 1.0
 */
public class GetUploads {

    public static UploadsApi getUploadsExample(AmazonAuthority authority) {
        AWSAuthenticationCredentials awsAuthenticationCredentials =
                AWSAuthenticationCredentials.builder()
                        .accessKeyId(authority.getAccessKeyId())
                        .secretKey(authority.getSecretKey())
                        .region(authority.getRegion())
                        .build();

        AWSAuthenticationCredentialsProvider awsAuthenticationCredentialsProvider = AWSAuthenticationCredentialsProvider.builder()
                .roleArn(authority.getRoleArn())
                .roleSessionName(authority.getRoleSessionName())
                .build();

        LWAAuthorizationCredentials lwaAuthorizationCredentials = LWAAuthorizationCredentials.builder()
                .clientId(authority.getClientId())
                .clientSecret(authority.getClientSecret())
                .refreshToken(authority.getRefreshToken())
                .endpoint(authority.getLwaEndPoint())
                .build();

        UploadsApi uploadsApi = new UploadsApi.Builder()
                .awsAuthenticationCredentials(awsAuthenticationCredentials)
                .awsAuthenticationCredentialsProvider(awsAuthenticationCredentialsProvider)
                .lwaAuthorizationCredentials(lwaAuthorizationCredentials)
                .endpoint(authority.getSpEndPoint())
                .build();

        return uploadsApi;
    }
}
