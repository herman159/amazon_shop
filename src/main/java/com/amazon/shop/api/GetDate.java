package com.amazon.shop.api;

import com.amazon.shop.spapi.api.OrdersV0Api;
import com.amazon.shop.spapi.api.ReportsApi;
import com.amazon.shop.spapi.client.ApiException;
import com.amazon.shop.spapi.model.orders.*;
import com.amazon.shop.spapi.model.reports.CreateReportResponse;
import com.amazon.shop.spapi.model.reports.CreateReportSpecification;
import com.amazon.shop.spapi.model.reports.Report;
import com.amazon.shop.spapi.model.reports.ReportDocument;

import java.util.List;

public class GetDate {

    public GetOrdersResponse getAmazonOrder(OrdersV0Api ordersV0Api, List<String> marketplaceIds, String createDate) throws ApiException {
        GetOrdersResponse response = ordersV0Api.getOrders(marketplaceIds
                , createDate
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null);
        //System.out.println(response.toString());
        return response;
    }

    public GetOrderBuyerInfoResponse getOrderBuyerInfoResponse(OrdersV0Api ordersV0Api, String orderId) throws ApiException {
        GetOrderBuyerInfoResponse response = ordersV0Api.getOrderBuyerInfo(orderId);

        return response;
    }

    public GetOrderAddressResponse getOrderAddressResponse(OrdersV0Api ordersV0Api, String orderId) throws ApiException {
        GetOrderAddressResponse response = ordersV0Api.getOrderAddress(orderId);

        return response;
    }

    public GetOrderItemsResponse getOrderItemsResponse(OrdersV0Api ordersV0Api, String orderId) throws ApiException {
        GetOrderItemsResponse response = ordersV0Api.getOrderItems(orderId, null);
        return response;
    }

    public GetOrderItemsBuyerInfoResponse getOrderItemsBuyerInfoResponse(OrdersV0Api ordersV0Api, String orderId) throws ApiException {
        GetOrderItemsBuyerInfoResponse response = ordersV0Api.getOrderItemsBuyerInfo(orderId, null);
        return response;
    }

    public CreateReportResponse createReportResponse(ReportsApi reportsApi, CreateReportSpecification createReportSpecification) throws ApiException {
        System.out.println("-----");
        CreateReportResponse response = reportsApi.createReport(createReportSpecification);
        return response;
    }

    public Report getReport(ReportsApi reportsApi, String report_id) throws ApiException {
        Report responce = reportsApi.getReport(report_id).getPayload();

        return responce;
    }

    public ReportDocument getReportDocument(ReportsApi reportsApi, String reportDocumentId) throws ApiException {
        ReportDocument responce = reportsApi.getReportDocument(reportDocumentId).getPayload();
        return responce;
    }

}
