package com.amazon.shop.api;

import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.spapi.SellingPartnerAPIAA.AWSAuthenticationCredentials;
import com.amazon.shop.spapi.SellingPartnerAPIAA.AWSAuthenticationCredentialsProvider;
import com.amazon.shop.spapi.SellingPartnerAPIAA.LWAAuthorizationCredentials;
import com.amazon.shop.spapi.api.FeedsApi;

/**
 * @Author 郭非
 * @Date 2021/6/16
 * @Time 16:23
 * @Version 1.0
 */
public class GetFeed {

    public static FeedsApi getFeedsExample(AmazonAuthority authority) {

        AWSAuthenticationCredentials awsAuthenticationCredentials =
                AWSAuthenticationCredentials.builder()
                        .accessKeyId(authority.getAccessKeyId())
                        .secretKey(authority.getSecretKey())
                        .region(authority.getRegion())
                        .build();

        AWSAuthenticationCredentialsProvider awsAuthenticationCredentialsProvider = AWSAuthenticationCredentialsProvider.builder()
                .roleArn(authority.getRoleArn())
                .roleSessionName(authority.getRoleSessionName())
                .build();

        LWAAuthorizationCredentials lwaAuthorizationCredentials = LWAAuthorizationCredentials.builder()
                .clientId(authority.getClientId())
                .clientSecret(authority.getClientSecret())
                .refreshToken(authority.getRefreshToken())
                .endpoint(authority.getLwaEndPoint())
                .build();

        FeedsApi feedsApi = new FeedsApi.Builder()
                .awsAuthenticationCredentials(awsAuthenticationCredentials)
                .awsAuthenticationCredentialsProvider(awsAuthenticationCredentialsProvider)
                .lwaAuthorizationCredentials(lwaAuthorizationCredentials)
                .endpoint(authority.getSpEndPoint())
                .build();

        return feedsApi;
    }
}
