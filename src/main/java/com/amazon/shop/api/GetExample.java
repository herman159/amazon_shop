package com.amazon.shop.api;

import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.spapi.SellingPartnerAPIAA.AWSAuthenticationCredentials;
import com.amazon.shop.spapi.SellingPartnerAPIAA.AWSAuthenticationCredentialsProvider;
import com.amazon.shop.spapi.SellingPartnerAPIAA.LWAAuthorizationCredentials;
import com.amazon.shop.spapi.api.OrdersV0Api;
import com.amazon.shop.spapi.api.ReportsApi;

public class GetExample {

    public static OrdersV0Api getExampleOrder(AmazonAuthority authority) {
        AWSAuthenticationCredentials awsAuthenticationCredentials =
                AWSAuthenticationCredentials.builder()
                        .accessKeyId(authority.getAccessKeyId())
                        .secretKey(authority.getSecretKey())
                        .region(authority.getRegion())
                        .build();

        AWSAuthenticationCredentialsProvider awsAuthenticationCredentialsProvider = AWSAuthenticationCredentialsProvider.builder()
                .roleArn(authority.getRoleArn())
                .roleSessionName(authority.getRoleSessionName())
                .build();

        LWAAuthorizationCredentials lwaAuthorizationCredentials = LWAAuthorizationCredentials.builder()
                .clientId(authority.getClientId())
                .clientSecret(authority.getClientSecret())
                .refreshToken(authority.getRefreshToken())
                .endpoint(authority.getLwaEndPoint())
                .build();


        OrdersV0Api ordersV0Api = new OrdersV0Api.Builder()
                .awsAuthenticationCredentials(awsAuthenticationCredentials)
                .awsAuthenticationCredentialsProvider(awsAuthenticationCredentialsProvider)
                .lwaAuthorizationCredentials(lwaAuthorizationCredentials)
                .endpoint(authority.getSpEndPoint())
                .build();
        if (null == ordersV0Api) {
            throw new RuntimeException();
        }

        return ordersV0Api;
    }

    public static ReportsApi getReports(AmazonAuthority authority) {
        AWSAuthenticationCredentials awsAuthenticationCredentials =
                AWSAuthenticationCredentials.builder()
                        .accessKeyId(authority.getAccessKeyId())
                        .secretKey(authority.getSecretKey())
                        .region(authority.getRegion())
                        .build();

        AWSAuthenticationCredentialsProvider awsAuthenticationCredentialsProvider = AWSAuthenticationCredentialsProvider.builder()
                .roleArn(authority.getRoleArn())
                .roleSessionName(authority.getRoleSessionName())
                .build();

        LWAAuthorizationCredentials lwaAuthorizationCredentials = LWAAuthorizationCredentials.builder()
                .clientId(authority.getClientId())
                .clientSecret(authority.getClientSecret())
                .refreshToken(authority.getRefreshToken())
                .endpoint(authority.getLwaEndPoint())
                .build();

        ReportsApi reportsApi = new ReportsApi.Builder()
                .awsAuthenticationCredentials(awsAuthenticationCredentials)
                .awsAuthenticationCredentialsProvider(awsAuthenticationCredentialsProvider)
                .lwaAuthorizationCredentials(lwaAuthorizationCredentials)
                .endpoint(authority.getSpEndPoint())
                .build();

        if (reportsApi == null) {
            throw new RuntimeException();
        }
        return reportsApi;
    }
}
