package com.amazon.shop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@MapperScan(basePackages = {"com.amazon.shop.mapper"})
@EnableTransactionManagement
public class AmazonShopApplication {
    public static void main(String[] args) {
        SpringApplication.run(AmazonShopApplication.class, args);
    }
}
