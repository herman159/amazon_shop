package com.amazon.shop.aop;

import java.lang.annotation.*;

/**
 * 自定义日志注解
 *
 * @Author 郭非
 * @Date 2021/6/15
 * @Time 10:27
 * @Version 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemLog {

    String value() default "";

}
