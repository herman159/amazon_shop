package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Tort对象", description = "")
public class Tort implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "侵权词编号")
    @TableId(value = "tort_id", type = IdType.INPUT)
    private String tortId;

    @ApiModelProperty(value = "侵权词")
    private String tortWord;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "替换词")
    private String replaceWord;

    @ApiModelProperty(value = "逻辑删除(0未删除 1已删除)")
    @TableLogic
    private Integer logicDeleted;


}
