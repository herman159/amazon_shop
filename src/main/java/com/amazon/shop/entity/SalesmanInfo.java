package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-07-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "SalesmanInfo对象", description = "")
public class SalesmanInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "业务员编号")
    @TableId(value = "number", type = IdType.INPUT)
    private String number;

    @ApiModelProperty(value = "保底工资")
    private BigDecimal minWages;

    @ApiModelProperty(value = "工资提成")
    private BigDecimal wageStatus;

    @ApiModelProperty(value = "销售利润提成")
    private BigDecimal profitCommission;

    @ApiModelProperty(value = "采购提成")
    private BigDecimal purchaseCommission;

    @ApiModelProperty(value = "物流提成")
    private BigDecimal logisticsCommission;

    @ApiModelProperty(value = "开发")
    private String develop;

    @ApiModelProperty(value = "范围")
    private String scope;

    @ApiModelProperty(value = "采购")
    private String shopping;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

}
