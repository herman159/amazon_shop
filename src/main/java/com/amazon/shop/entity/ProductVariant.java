package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ProductVariant对象", description = "")
public class ProductVariant implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "variant_id", type = IdType.INPUT)
    private String variantId;

    private String variant;

    private String productId;

    private String us;

    private String ca;

    private String mx;

    private String gb;

    private String fr;

    private String de;

    private String nl;

    private String it;

    @ApiModelProperty(value = "替换")
    private String productReplace;

    @TableLogic
    private Integer logicDeleted;


}
