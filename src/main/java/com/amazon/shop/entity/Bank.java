package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Bank对象", description = "")
public class Bank implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "银行编号")
    @TableId(value = "bank_id", type = IdType.INPUT)
    private String bankId;

    @ApiModelProperty(value = "公司编号")
    private String companyId;

    @ApiModelProperty(value = "银行名称")
    private String bank;

    @ApiModelProperty(value = "用户姓名")
    private String userName;

    @ApiModelProperty(value = "账户")
    private String bankAccount;

    @ApiModelProperty(value = "找货")
    private String findGoods;

    @ApiModelProperty(value = "支付方式")
    private String type;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

}
