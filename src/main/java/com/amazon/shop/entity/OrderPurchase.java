package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "OrderPurchase对象", description = "")
public class OrderPurchase implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "采购单号")
    @TableId(value = "odd_number", type = IdType.ASSIGN_UUID)
    private String oddNumber;

    @ApiModelProperty(value = "商品图片")
    private String pixMap;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "变体")
    private String variant;

    @ApiModelProperty(value = "转运商姓名")
    private String transportName;

    @ApiModelProperty(value = "qq")
    private String qq;

    @ApiModelProperty(value = "转运中心")
    private String transportCenter;

    @ApiModelProperty(value = "币种")
    private String moneyType;

    @ApiModelProperty(value = "费用")
    private BigDecimal money;

    @ApiModelProperty(value = "追踪单号")
    private String trackNumber;

    @ApiModelProperty(value = "签收人员")
    private String signName;

    @ApiModelProperty(value = "入库时间")
    private Date placeTime;

    @ApiModelProperty(value = "检查类型")
    private String checkType;

    @ApiModelProperty(value = "物品类型")
    private String productType;

    @ApiModelProperty(value = "采购网站")
    private String purchaseWebsite;

    @ApiModelProperty(value = "订单编号")
    private String orderId;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

    @ApiModelProperty(value = "备注")
    private String note;

}
