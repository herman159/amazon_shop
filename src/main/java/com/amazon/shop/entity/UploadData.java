package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-07-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "UploadData对象", description = "")
public class UploadData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键编号")
    @TableId(value = "upload_id", type = IdType.INPUT)
    private String uploadId;

    @ApiModelProperty(value = "提要编号")
    private String feedId;

    @ApiModelProperty(value = "业务员")
    private String salesman;

    @ApiModelProperty(value = "数量")
    private String count;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "错误")
    private String error;

    @ApiModelProperty(value = "警告")
    private String warning;

    @ApiModelProperty(value = "编号")
    private String amazonId;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
