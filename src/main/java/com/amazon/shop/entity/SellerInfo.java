package com.amazon.shop.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SellerInfo对象", description="")
public class SellerInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "销售单号")
    @TableId(value = "seller_num", type = IdType.INPUT)
    private String sellerNum;

    @ApiModelProperty(value = "账户")
    private String sellerAccount;

    @ApiModelProperty(value = "订单费用")
    private BigDecimal orderPrice;

    @ApiModelProperty(value = "手续费")
    private BigDecimal serviceCharge;

    @ApiModelProperty(value = "金额(结余)")
    private BigDecimal money;

}
