package com.amazon.shop.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Transfer对象", description="")
public class Transfer implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "转账充值编号")
    @TableId(value = "transfer_id", type = IdType.INPUT)
    private String transferId;

    @ApiModelProperty(value = "转入账户")
    private String inAccount;

    @ApiModelProperty(value = "转出账户")
    private String outAccount;

    @ApiModelProperty(value = "金额")
    private BigDecimal money;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "录入员")
    private String username;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

}
