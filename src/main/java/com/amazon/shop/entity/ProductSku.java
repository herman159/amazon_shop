package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ProductSku对象", description = "")
public class ProductSku implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品sku编号")
    @TableId(type = IdType.INPUT)
    private String skuId;

    @ApiModelProperty(value = "产品sku")
    private String sku;

    @ApiModelProperty(value = "产品编号")
    private String productId;

    @ApiModelProperty(value = "逻辑删除(0未删除 1已删除)")
    @TableLogic
    private Integer logicDeleted;


}
