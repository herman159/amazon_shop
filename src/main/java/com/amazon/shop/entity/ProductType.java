package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ProductType对象", description = "")
public class ProductType implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品类型编号")
    @TableId(value = "product_type_id", type = IdType.INPUT)
    private String productTypeId;

    @ApiModelProperty(value = "产品一级分类")
    private String productTypeOne;

    @ApiModelProperty(value = "产品二级分类")
    private String productTypeTwo;

    @ApiModelProperty(value = "产品三级分类")
    private String productTypeThree;

    @ApiModelProperty(value = "产品四级分类")
    private String productTypeFour;

    @ApiModelProperty(value = "产品五级分类")
    private String productTypeFive;

    @ApiModelProperty(value = "产品六级分类")
    private String productTypeSix;


}
