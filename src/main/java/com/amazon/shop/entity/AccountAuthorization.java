package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "AccountAuthorization对象", description = "")
public class AccountAuthorization implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "站点编号")
    @TableId(value = "number", type = IdType.INPUT)
    private String number;

    @ApiModelProperty(value = "授权账户")
    private String authorizationAccount;

    @ApiModelProperty(value = "启用")
    private String enable;

    @ApiModelProperty(value = "站点名")
    private String shopName;

    @ApiModelProperty(value = "编码类型")
    private String codeType;

    @ApiModelProperty(value = "独立编码")
    private String independentCode;

    @ApiModelProperty(value = "备货天数")
    private String dayNumber;

    @ApiModelProperty(value = "基准账户")
    private String benchmarkAccount;

    @ApiModelProperty(value = "fba是否开启")
    @TableField("FBA_enable")
    private String fbaEnable;

    @ApiModelProperty(value = "百分比")
    private String percentage;

    @ApiModelProperty(value = "业务员编号")
    private String salesmanId;

    @ApiModelProperty(value = "资料归属")
    private String belongId;

    @ApiModelProperty(value = "登录帐号")
    private String loginId;

    @ApiModelProperty(value = "信用卡号")
    private String creditCardNumber;

    @ApiModelProperty(value = "国家编号")
    private String countryId;

    @ApiModelProperty(value = "国家名称")
    private String countryName;

    @ApiModelProperty(value = "国家分组")
    private String countryGroup;

    @ApiModelProperty(value = "开发商名称")
    private String manufacturerNumber;

    @ApiModelProperty(value = "卖家编号")
    private String sellerNumber;

    @ApiModelProperty(value = "授权令牌")
    private String authorizationToken;

    @ApiModelProperty(value = "货币种类")
    private String currencyType;

    @ApiModelProperty(value = "最小价格")
    private BigDecimal minimumSellingPrice;

    @ApiModelProperty(value = "最大价格")
    private BigDecimal maximumSellingPrice;

    @ApiModelProperty(value = "调整")
    private String adjust;

    @ApiModelProperty(value = "执照法人")
    private String legalPerson;

    @ApiModelProperty(value = "公司名称")
    private String companyName;

    @ApiModelProperty(value = "p卡或wf")
    private String pCardOrWfCard;

    @ApiModelProperty(value = "vps地址")
    private String vpsAddress;

    @ApiModelProperty(value = "vps用户")
    private String vpsUser;

    @ApiModelProperty(value = "vps密码")
    private String vpsPassword;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

}
