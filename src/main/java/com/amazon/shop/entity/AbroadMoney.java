package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "AbroadMoney对象", description = "")
public class AbroadMoney implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键编号")
    @TableId(value = "abroad_money_id", type = IdType.INPUT)
    private String abroadMoneyId;

    @ApiModelProperty(value = "录入员")
    private String username;

    @ApiModelProperty(value = "账户")
    private String bankAccount;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "汇入钱数")
    private BigDecimal money;

    @ApiModelProperty(value = "兑换钱数")
    private BigDecimal exchangeMoney;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "币种编号")
    private String currencyId;

    @ApiModelProperty(value = "国家编号")
    private String countryId;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
