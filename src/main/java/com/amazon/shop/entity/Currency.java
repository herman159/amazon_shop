package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Currency对象", description="")
public class Currency implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "币种编号")
    @TableId(value = "currency_id", type = IdType.INPUT)
    private String currencyId;

    @ApiModelProperty(value = "币种")
    private String currencyName;


}
