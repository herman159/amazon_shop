package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Product对象", description = "")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品编号")
    @TableId(value = "numbers", type = IdType.INPUT)
    private String numbers;

    @ApiModelProperty(value = "产品分类编号")
    private String productTypeId;

    @ApiModelProperty(value = "审核状态")
    private String status;

    @ApiModelProperty(value = "上架下架")
    private String upDown;

    @ApiModelProperty(value = "安全等级")
    private String safeLevel;

    @ApiModelProperty(value = " sku编号")
    private String skuId;

    @ApiModelProperty(value = "产品等级")
    private String productLevel;

    @ApiModelProperty(value = "产品开发")
    private String development;

    @ApiModelProperty(value = "美工修图")
    private String beautifyPic;

    @ApiModelProperty(value = "产品品牌")
    private String brand;

    @ApiModelProperty(value = "厂商名")
    private String manuName;

    @ApiModelProperty(value = "厂商编号")
    private String manuNum;

    @ApiModelProperty(value = "原产地")
    private String origin;

    @ApiModelProperty(value = "产品目录")
    private String catalog;

    @ApiModelProperty(value = "海关编号")
    private Integer customsNum;

    @ApiModelProperty(value = "中文简称")
    private String znName;

    @ApiModelProperty(value = "英文简称")
    private String cnName;

    @ApiModelProperty(value = "申报单价")
    private BigDecimal declarePrice;

    @ApiModelProperty(value = "包装毛重")
    private String weigh;

    @ApiModelProperty(value = "长")
    private Double productL;

    @ApiModelProperty(value = "高")
    private String productH;

    @ApiModelProperty(value = "宽")
    private String productW;

    @ApiModelProperty(value = "特殊类型")
    private String specialType;

    @ApiModelProperty(value = "产品币种")
    private String currency;

    @ApiModelProperty(value = "成本单价")
    private BigDecimal costPrice;

    @ApiModelProperty(value = "固定运费")
    private BigDecimal freight;

    @ApiModelProperty(value = "国家运费")
    private BigDecimal countryFreight;

    @ApiModelProperty(value = "商品视频")
    private String videoWeb;

    @ApiModelProperty(value = "供应商名")
    private String supplierName;

    @ApiModelProperty(value = "供应货号")
    private String supplierNum;

    @ApiModelProperty(value = "采集网站")
    private String collectionWeb;

    @ApiModelProperty(value = "采集状态(0已采集 1已入库)")
    private Integer collectionStatus;

    @ApiModelProperty(value = "产品备注")
    private String note;

    @ApiModelProperty(value = "适合人群")
    private String fitPeople;

    @ApiModelProperty(value = "年龄分组")
    private String fitAge;

    @ApiModelProperty(value = "产品材料")
    private String material;

    @ApiModelProperty(value = "外部材料")
    private String outMaterial;

    @ApiModelProperty(value = "金属类型")
    private String metal;

    @ApiModelProperty(value = "珠宝类型")
    private String jewelry;

    @ApiModelProperty(value = "鞋类宽度")
    private String shoeSize;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

}
