package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "OrderSimple对象", description = "")
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单号")
    @TableId(value = "order_number", type = IdType.INPUT)
    private String orderNumber;

    @ApiModelProperty(value = "亚马逊状态")
    private String status;

    @ApiModelProperty(value = "订单状态")
    private String orderStatus;

    @ApiModelProperty(value = "订单子状态")
    private String orderChildStatus;

    @ApiModelProperty(value = "买家编号")
    private String buyerId;

    @ApiModelProperty(value = "卖家编号")
    private String sellerId;

    @ApiModelProperty(value = "采购单号")
    private String amazonOrderId;

    @ApiModelProperty(value = "商品编号")
    private String commodityId;

    @ApiModelProperty(value = "运单号")
    private String wayBillNumber;

    @ApiModelProperty(value = "备注")
    private String contentNotes;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

    @ApiModelProperty(value = "父异常状态(是否具有异常)")
    private String fatherError;

    @ApiModelProperty(value = "子异常状态 (异常具体类型)")
    private String childError;

    @ApiModelProperty(value = "销售员编号")
    private String salesmanId;

}
