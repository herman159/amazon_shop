package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Country对象", description="")
public class Country implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "国家编号")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty(value = "国家名称")
    private String name;

    @ApiModelProperty(value = "国家分组")
    private String countryGroup;

    private String spEndpoint;

    @ApiModelProperty(value = "aws区域")
    private String aws;

    private String marketplaceId;

    @ApiModelProperty(value = "国家地区代码")
    private String countryCode;

    @ApiModelProperty(value = "逻辑删除(0未删除 1已删除)")
    @TableLogic
    private Integer logicDeleted;

}
