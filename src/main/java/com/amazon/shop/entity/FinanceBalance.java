package com.amazon.shop.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="FinanceBalance对象", description="")
public class FinanceBalance implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "公司编号")
    @TableId(value = "apartment_id", type = IdType.INPUT)
    private String apartmentId;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "可提现")
    private BigDecimal withdrawal;

    @ApiModelProperty(value = "申请提现")
    private BigDecimal applyPrice;

}
