package com.amazon.shop.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ReturnOrder对象", description="")
public class ReturnOrder implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "订单号")
    @TableId(value = "order_number", type = IdType.ASSIGN_UUID)
    private String orderNumber;

    @ApiModelProperty(value = "图片")
    private String pixMap;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    @ApiModelProperty(value = "协同号")
    private String accountNumber;

    @ApiModelProperty(value = "存储")
    private String storage;

    @ApiModelProperty(value = "仓库")
    private String warehouse;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "sku")
    private String sku;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
