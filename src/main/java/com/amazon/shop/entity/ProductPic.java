package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ProductPic对象", description = "")
public class ProductPic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "图片编号")
    @TableId(value = "image_id", type = IdType.ASSIGN_UUID)
    private String imageId;

    @ApiModelProperty(value = "sku")
    private String sku;

    @ApiModelProperty(value = "产品编号")
    private String productId;

    @ApiModelProperty(value = "图片Oss地址")
    private String path;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
