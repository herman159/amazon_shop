package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Cooperative对象", description="")
public class Cooperative implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "合作伙伴编号")
    @TableId(value = "cooperative_id", type = IdType.INPUT)
    private String cooperativeId;

    @ApiModelProperty(value = "协同号")
    private Integer number;

    @ApiModelProperty(value = "是否允许分销")
    private String isEnableSale;

    @ApiModelProperty(value = "比例")
    private String proportion;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "qq")
    private String qq;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String mail;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
