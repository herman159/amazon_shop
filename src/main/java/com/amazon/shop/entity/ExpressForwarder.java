package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExpressForwarder对象", description="")
public class ExpressForwarder implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "协同号")
    @TableId(value = "number", type = IdType.INPUT)
    private String number;

    @ApiModelProperty(value = "是否允许")
    private String isEnable;

    @ApiModelProperty(value = "联络人")
    private String contacts;

    @ApiModelProperty(value = "公司名称")
    private String company;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "qq")
    private String qq;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "利润")
    private BigDecimal profit;


}
