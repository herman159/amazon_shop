package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OrderBack对象", description="")
public class OrderBack implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "订单号")
    @TableId(value = "order_id", type = IdType.INPUT)
    private String orderId;

    @ApiModelProperty(value = "订单状态")
    private String orderStatus;

    @ApiModelProperty(value = "订单子状态")
    private String orderChildStatus;

    @ApiModelProperty(value = "运单号")
    private String wayBillNumber;

    @ApiModelProperty(value = "销售员")
    private String salesmanId;

    @ApiModelProperty(value = "备注")
    private String contentNotes;

    @ApiModelProperty(value = "订单父异常")
    private String fatherError;

    @ApiModelProperty(value = "订单子异常")
    private String childError;


}
