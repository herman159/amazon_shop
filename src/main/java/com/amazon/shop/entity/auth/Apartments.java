package com.amazon.shop.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Apartments对象", description = "")
public class Apartments implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "部门主键")
    @TableId(value = "apartment_id", type = IdType.INPUT)
    private String apartmentId;

    @ApiModelProperty(value = "父类部门编号")
    private String parentId;

    @ApiModelProperty(value = "部门名称")
    private String apartmentName;

    @ApiModelProperty(value = "公司名称")
    private String company;

    @ApiModelProperty(value = "是否可见")
    private String isView;

    @ApiModelProperty(value = "是否禁用")
    private String isDisable;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "限额")
    private String minBalance;

    @ApiModelProperty(value = "允许其他部门")
    private String isShare;

    @ApiModelProperty(value = "允许本部门")
    private String isSuperior;

    @ApiModelProperty(value = "逻辑删除(0未删除 1已删除)")
    @TableLogic
    private Integer logicDeleted;

}
