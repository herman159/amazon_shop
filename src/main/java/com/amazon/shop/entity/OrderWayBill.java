package com.amazon.shop.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OrderWayBill对象", description="")
public class OrderWayBill implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "面单号")
    @TableId(value = "way_bill_number", type = IdType.INPUT)
    private String wayBillNumber;

    @ApiModelProperty(value = "图片")
    private String pixMap;

    @ApiModelProperty(value = "转运商姓名")
    private String purchaseName;

    @ApiModelProperty(value = "转运商")
    private String purchaseCenter;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "称重")
    private String widget;

    @ApiModelProperty(value = "二次称重")
    private String widgetTwo;

    @ApiModelProperty(value = "物流")
    private String express;

    @ApiModelProperty(value = "追踪号")
    private String number;

    @ApiModelProperty(value = "编号类型")
    private String numberType;

    @ApiModelProperty(value = "发货日期")
    private Date deliveryDate;

    @ApiModelProperty(value = "费用")
    private BigDecimal price;

    @ApiModelProperty(value = "订单号")
    private String aboutOrder;

    @ApiModelProperty(value = "销售人员")
    private String salesman;

    @ApiModelProperty(value = "英文名")
    private String enTitle;

    @ApiModelProperty(value = "中文名")
    private String cnTitle;

    @ApiModelProperty(value = "币种")
    private String declares;

    @ApiModelProperty(value = "申报币种")
    private String declareType;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;

}
