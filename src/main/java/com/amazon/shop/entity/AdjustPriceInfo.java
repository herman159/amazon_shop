package com.amazon.shop.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AdjustPriceInfo对象", description="")
public class AdjustPriceInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "产品分类编号")
    private String typeId;

    @ApiModelProperty(value = "比例")
    private String proportion;

    @ApiModelProperty(value = "币种")
    private String currency;

    @ApiModelProperty(value = "起始金额")
    private BigDecimal startAmount;

    @ApiModelProperty(value = "微调")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "逻辑删除(0未删除 1已删除)")
    @TableLogic
    private Integer logicDeleted;

}
