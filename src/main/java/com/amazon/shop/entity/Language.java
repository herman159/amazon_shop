package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Language对象", description = "")
public class Language implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "语言编号")
    @TableId(value = "language_id", type = IdType.ASSIGN_UUID)
    private String languageId;

    @ApiModelProperty(value = "语言")
    private String language;

    @ApiModelProperty(value = "公司编号")
    private String companyId;

    @ApiModelProperty(value = "逻辑删除(0未删除 1已删除)")
    @TableLogic
    private Integer logicDeleted;

}
