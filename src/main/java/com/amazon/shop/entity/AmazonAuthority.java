package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "AmazonAuthority对象", description = "")
public class AmazonAuthority implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)

    @ApiModelProperty(value = "站点编号")
    private String id;

    @ApiModelProperty(value = "AWS访问密钥编码")
    private String accessKeyId;

    @ApiModelProperty(value = "AWS访问密钥")
    private String secretKey;

    @ApiModelProperty(value = "区域")
    private String region;

    @ApiModelProperty(value = "IAM职权ARN")
    private String roleArn;

    @ApiModelProperty(value = "IAM职权名称")
    private String roleSessionName;

    @ApiModelProperty(value = "LWA客户端编码")
    private String clientId;

    @ApiModelProperty(value = "LWA客户端秘钥")
    private String clientSecret;

    @ApiModelProperty(value = "LWA客户端令牌")
    private String refreshToken;

    @ApiModelProperty(value = "SP授权服务器节点地址")
    private String spEndPoint;

    @ApiModelProperty(value = "LWA授权服务器的节点地址")
    private String lwaEndPoint;

    @TableLogic
    private Integer logicDeleted;


}
