package com.amazon.shop.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "TitleWord对象", description = "")
public class TitleWord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品标题编号")
    @TableId(value = "title_word_id", type = IdType.INPUT)
    private String titleWordId;

    @ApiModelProperty(value = "产品编号")
    private String productId;

    @ApiModelProperty(value = "产品分类编号")
    private String typeId;

    @ApiModelProperty(value = "是否开启")
    private String isEnable;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
