package com.amazon.shop.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OfficeCost对象", description="")
public class OfficeCost implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键编号")
    @TableId(value = "office_cost_id", type = IdType.INPUT)
    private String officeCostId;

    @ApiModelProperty(value = "账户")
    private String account;

    @ApiModelProperty(value = "用途")
    private String uses;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "支出")
    private BigDecimal expense;

    @ApiModelProperty(value = "录入员")
    private String username;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "逻辑删除(0未删除 1未删除)")
    @TableLogic
    private Integer logicDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private String gmtCreated;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String gmtModified;


}
