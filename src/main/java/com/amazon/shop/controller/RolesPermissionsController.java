package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.auth.Roles;
import com.amazon.shop.entity.auth.RolesPermissions;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.RolesPermissionsService;
import com.amazon.shop.service.RolesService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/shop/roles-permissions")
@Api(tags = "角色权限模块")
public class RolesPermissionsController {
    @Resource
    private RolesPermissionsService rolesPermissionsService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private RolesService rolesService;

    /**
     * 新增角色权限
     *
     * @param permissions
     * @param role
     * @param token
     * @return
     */
    @SystemLog(value = "新增角色权限")
    @PostMapping("/saveNewPermissions")
    @ApiOperation(value = "新增角色权限")
    @Transactional
    public CommonResult saveNewPermissions(@RequestBody List<String> permissions
            , String role, String apartment, String token) {
        boolean flag = permissionsService.getPermissionList(token, "2");
        if (flag) {
            //根据角色名查询角色编号
            QueryWrapper<Roles> rolesQueryWrapper = new QueryWrapper<>();
            rolesQueryWrapper.eq("role_name", role);
            rolesQueryWrapper.eq("apartment_name", apartment);
            Roles roles = rolesService.getOne(rolesQueryWrapper);

            //保存角色权限列表
            String roleId = roles.getRoleId();
            List<RolesPermissions> rolesPermissionsList = permissions.stream().map((permission) -> {
                RolesPermissions rolesPermissions = new RolesPermissions();
                //自定义主键
                String id = UUID.randomUUID().toString().replace("-", "");
                rolesPermissions.setId(id);
                rolesPermissions.setRoleId(roleId);
                rolesPermissions.setPermissionId(permission);
                return rolesPermissions;
            }).collect(Collectors.toList());

            boolean saveBatch = rolesPermissionsService.saveOrUpdateBatch(rolesPermissionsList);
            if (saveBatch) {
                //保存的主键集合
                List<String> collect = rolesPermissionsList.stream().map((rolesPermission) -> {
                    String id = rolesPermission.getId();
                    return id;
                }).collect(Collectors.toList());
                //业务员姓名
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("保存角色权限列表成功")
                        .put("list", collect)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("保存角色权限列表失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

