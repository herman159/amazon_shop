package com.amazon.shop.controller;


import com.amazon.shop.service.UsersRolesService;
import com.amazon.shop.utils.CommonResult;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/shop/users-roles")
public class UsersRolesController {
}

