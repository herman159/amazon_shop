package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.ProductPic;
import com.amazon.shop.entity.ReturnOrder;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.ProductPicService;
import com.amazon.shop.service.ReturnOrderService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.ReturnOrderVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/return-order")
@Api(tags = "退货单模块")
public class ReturnOrderController {
    @Resource
    private ReturnOrderService returnOrderService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private ProductPicService productPicService;
    @Resource
    private UsersServiceImpl usersService;

    @SystemLog(value = "添加退货单")
    @PostMapping("/addReturnOrder")
    @ApiOperation(value = "添加退货单")
    @Transactional
    public CommonResult addReturnOrder(@RequestBody ReturnOrderVo returnOrderVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "39");
        if (flag) {
            ReturnOrder returnOrder = new ReturnOrder();
            BeanUtils.copyProperties(returnOrderVo, returnOrder);
            String orderNumber = returnOrderVo.getOrderNumber();
            //通过sku查询处理图片
            String sku = returnOrderVo.getSku();
            QueryWrapper<ProductPic> productPicQueryWrapper = new QueryWrapper<>();
            productPicQueryWrapper.eq("sku", sku);
            ProductPic productPic = productPicService.getOne(productPicQueryWrapper);
            String pixMap = productPic.getPath();
            returnOrder.setPixMap(pixMap);
            //处理时间
            Date addTime = returnOrderVo.getAddTime();
            returnOrder.setGmtCreated(addTime.toString());
            boolean save = returnOrderService.save(returnOrder);
            if (save) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("添加退货单成功")
                        .put("list", orderNumber)
                        .put("salesman", userRealName);
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    @GetMapping("/getReturnOrder")
    @ApiOperation(value = "获取退货单列表")
    public CommonResult getReturnOrder(String token) {
        boolean flag = permissionsService.getPermissionList(token, "49");
        if (flag) {
            List<ReturnOrder> returnOrders = returnOrderService.list();
            return CommonResult.success("获取退货单列表成功").put("returnOrders", returnOrders);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

