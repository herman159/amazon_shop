package com.amazon.shop.controller;


import com.amazon.shop.entity.Cooperative;
import com.amazon.shop.service.CooperativeService;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.CooperativeSearchVo;
import com.amazon.shop.vo.CooperativeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/shop/cooperative")
@Api(tags = "合作伙伴模块")
public class CooperativeController {
    @Resource
    private CooperativeService cooperativeService;

    /**
     * 保存合作伙伴
     *
     * @param cooperativeVo
     * @return
     */
    @PostMapping("/saveCooperative")
    @ApiOperation(value = "保存合作伙伴")
    public CommonResult saveCooperative(@RequestBody CooperativeVo cooperativeVo) {
        Cooperative cooperative = new Cooperative();
        //属性对拷
        BeanUtils.copyProperties(cooperativeVo, cooperative);

        //自定义编号
        String cooperativeId = UUID.randomUUID().toString().replace("-", "");
        cooperative.setCooperativeId(cooperativeId);

        //保存信息
        boolean save = cooperativeService.save(cooperative);
        if (save) {
            return CommonResult.success("保存合作伙伴成功");
        } else {
            return CommonResult.error("保存合作伙伴失败");
        }
    }

    /**
     * 查询合作伙伴列表
     *
     * @return
     */
    @GetMapping("/getCooperative")
    @ApiOperation(value = "查询合作伙伴列表")
    public CommonResult getCooperative() {
        List<Cooperative> cooperativeList = cooperativeService.list();
        List<CooperativeSearchVo> cooperativeSearchVos = cooperativeList.stream().map((cooperative) -> {
            CooperativeSearchVo cooperativeSearchVo = new CooperativeSearchVo();
            BeanUtils.copyProperties(cooperative, cooperativeSearchVo);
            return cooperativeSearchVo;
        }).collect(Collectors.toList());
        return CommonResult.success("查询合作伙伴列表成功").put("cooperativeSearchVos", cooperativeSearchVos);
    }

    /**
     * 删除合作伙伴
     *
     * @param cooperativeId
     * @return
     */
    @PostMapping("/removeCooperative")
    @ApiOperation(value = "删除合作伙伴")
    public CommonResult removeCooperative(String cooperativeId) {
        boolean remove = cooperativeService.removeById(cooperativeId);
        if (remove) {
            return CommonResult.success("删除合作伙伴成功");
        } else {
            return CommonResult.error("删除合作伙伴失败");
        }
    }
}

