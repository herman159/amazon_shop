package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.auth.Permissions;
import com.amazon.shop.entity.auth.Roles;
import com.amazon.shop.entity.auth.RolesPermissions;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.RolesPermissionsService;
import com.amazon.shop.service.RolesService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.RolesSaveVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/shop/roles")
@Api(tags = "角色模块")
public class RolesController {
    @Resource
    private RolesService rolesService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private RolesPermissionsService rolesPermissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 新增角色
     *
     * @return
     */
    @SystemLog(value = "新增角色")
    @PostMapping("/saveRoles")
    @ApiOperation(value = "新增角色")
    @Transactional
    public CommonResult saveRoles(@RequestBody RolesSaveVo rolesSaveVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "2");
        if (flag) {
            //自定义角色编号
            String replace = UUID.randomUUID().toString().replace("-", "");
            String rolesId = replace + "roles";
            String rolesName = rolesSaveVo.getRolesName();

            //保存角色信息
            String apartment = rolesSaveVo.getApartment();
            Roles roles = new Roles();
            roles.setRoleId(rolesId);
            roles.setRoleName(rolesName);
            roles.setApartmentName(apartment);
            rolesService.save(roles);

            //保存角色权限列表信息
            List<Permissions> permissionsList = rolesSaveVo.getPermissionsList();
            List<RolesPermissions> rolesPermissionsList = permissionsList.stream().map((permission) -> {
                RolesPermissions rolesPermissions = new RolesPermissions();
                String permissionId = permission.getPermissionId();
                rolesPermissions.setPermissionId(permissionId);
                rolesPermissions.setRoleId(rolesId);
                return rolesPermissions;
            }).collect(Collectors.toList());
            //批量保存角色权限信息
            rolesPermissionsService.saveOrUpdateBatch(rolesPermissionsList);

            //获取业务员姓名
            String userRealName = usersService.getUserRealName(token);

            return CommonResult.success("新增角色成功")
                    .put("list", rolesId)
                    .put("salesman", userRealName);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

