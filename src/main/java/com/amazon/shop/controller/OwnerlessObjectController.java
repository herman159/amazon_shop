package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.OwnerlessObject;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.OwnerlessObjectService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-21
 */
@RestController
@RequestMapping("/shop/ownerless-object")
@Api(tags = "无主物模块")
public class OwnerlessObjectController {
    @Resource
    private OwnerlessObjectService ownerlessObjectService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 获取无主物列表
     *
     * @return
     */
    @GetMapping("/getOwnerlessObjectList")
    @ApiOperation(value = "获取无主物列表")
    public CommonResult getOwnerlessObjectList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "49");
        if (flag) {
            List<OwnerlessObject> ownerlessObjectList = ownerlessObjectService.list();
            if (ownerlessObjectList.size() > 0) {
                return CommonResult.success("获取无主物列表成功")
                        .put("ownerlessObjectList", ownerlessObjectList);
            } else {
                return CommonResult.error("获取无主物列表失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 签收无主物
     *
     * @param orderId
     * @return
     */
    @SystemLog(value = "签收无主物")
    @ApiOperation(value = "签收无主物")
    @PostMapping("/signOwnerlessObject")
    @Transactional
    public CommonResult signOwnerlessObject(String orderId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "49");
        if (flag) {
            QueryWrapper<OwnerlessObject> ownerlessObjectQueryWrapper = new QueryWrapper<>();
            ownerlessObjectQueryWrapper.eq("order_number", orderId);
            boolean remove = ownerlessObjectService.remove(ownerlessObjectQueryWrapper);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("签收无主物成功")
                        .put("list", orderId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.success("签收无主物失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

