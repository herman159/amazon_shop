package com.amazon.shop.controller;


import com.amazon.shop.entity.ProductSku;
import com.amazon.shop.service.ProductSkuService;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.ProductSkuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/shop/product-sku")
@Api(tags = "产品sku模块")
public class ProductSkuController {
    @Resource
    private ProductSkuService productSkuService;

    /**
     * 保存产品sku
     *
     * @param productSkuVo
     * @return
     */
    @PostMapping("/saveProductSku")
    @ApiOperation(value = "保存产品sku")
    public CommonResult saveProductSku(@RequestBody ProductSkuVo productSkuVo) {
        ProductSku productSku = new ProductSku();
        BeanUtils.copyProperties(productSkuVo, productSku);

        boolean save = productSkuService.save(productSku);
        if (save) {
            return CommonResult.success("保存成功");
        } else {
            return CommonResult.error("保存失败");
        }
    }
}

