package com.amazon.shop.controller;


import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.UploadData;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.UploadDataService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.UploadDataSearchVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-05
 */
@RestController
@RequestMapping("/shop/upload-data")
@Api(tags = "上传信息模块")
public class UploadDataController {
    @Resource
    private UploadDataService uploadDataService;
    @Resource
    private PermissionsServiceImpl permissionsService;

    /**
     * 获取上传信息列表
     *
     * @return
     */
    @GetMapping("/getUploadDataList")
    @ApiOperation(value = "获取上传信息列表")
    public CommonResult getUploadDataList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "24");
        if (flag) {
            List<UploadData> uploadDataList = uploadDataService.list();
            List<UploadDataSearchVo> uploadDataSearchVos = uploadDataList.stream().map((upload) -> {
                UploadDataSearchVo uploadDataSearchVo = new UploadDataSearchVo();
                Date submitTime = null;
                try {
                    String gmtCreated = upload.getGmtCreated();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    submitTime = dateFormat.parse(gmtCreated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //设置时间
                uploadDataSearchVo.setSubmitTime(submitTime);

                BeanUtils.copyProperties(upload, uploadDataSearchVo);
                return uploadDataSearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success("获取上传信息列表成功").put("uploadDataSearchVos", uploadDataSearchVos);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

