package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.AdjustPriceInfo;
import com.amazon.shop.entity.ProductType;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.AdjustPriceInfoService;
import com.amazon.shop.service.ProductTypeService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.AdjustPriceInfoVo;
import com.amazon.shop.vo.AdjustSearchVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/adjust-price-info")
@Api(tags = "价格调整模块")
public class AdjustPriceInfoController {
    @Resource
    private AdjustPriceInfoService adjustPriceInfoService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private ProductTypeService productTypeService;

    /**
     * 调整分销价格
     *
     * @param adjustPriceInfoVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增分销价格")
    @PostMapping("/saveAdjustPrice")
    @ApiOperation(value = "新增分销价格")
    @Transactional
    public CommonResult saveAdjustPrice(@RequestBody(required = false) AdjustPriceInfoVo adjustPriceInfoVo, String token) {
        //判断权限
        boolean flag = permissionsService.getPermissionList(token, "28");
        if (flag) {
            AdjustPriceInfo adjustPriceInfo = new AdjustPriceInfo();
            String replace = UUID.randomUUID().toString().replace("-", "");
            String id = replace + "adjust";
            //对应属性对拷
            BeanUtils.copyProperties(adjustPriceInfoVo, adjustPriceInfo);
            //获取产品分类
            //获取产品一级分类
            String productTypeOne = adjustPriceInfoVo.getProductTypeOne();
            //获取产品二级分类
            String productTypeTwo = adjustPriceInfoVo.getProductTypeTwo();
            //获取产品三级分类
            String productTypeThree = adjustPriceInfoVo.getProductTypeThree();
            //获取产品四级分类
            String productTypeFour = adjustPriceInfoVo.getProductTypeFour();
            //获取产品五级分类
            String productTypeFive = adjustPriceInfoVo.getProductTypeFive();
            //获取产品六级分类
            String productTypeSix = adjustPriceInfoVo.getProductTypeSix();
            //处理产品分类数据 通过产品分类查询产品分类编号
            QueryWrapper<ProductType> productTypeQueryWrapper = new QueryWrapper<>();
            if (!StringUtils.isEmpty(productTypeOne)) {
                productTypeQueryWrapper.and((wrapper) -> {
                    wrapper.eq("product_type_one", productTypeOne);
                    if (!StringUtils.isEmpty(productTypeTwo)) {
                        wrapper.eq("product_type_two", productTypeTwo);
                        if (!StringUtils.isEmpty(productTypeThree)) {
                            wrapper.eq("product_type_three", productTypeThree);
                            if (!StringUtils.isEmpty(productTypeFour)) {
                                wrapper.eq("product_type_four", productTypeFour);
                                if (!StringUtils.isEmpty(productTypeFive)) {
                                    wrapper.eq("product_type_five", productTypeFive);
                                    if (!StringUtils.isEmpty(productTypeSix)) {
                                        wrapper.eq("product_type_six", productTypeSix);
                                    }
                                }
                            }
                        }
                    }
                });
            }
            ProductType productType = productTypeService.getOne(productTypeQueryWrapper);
            //获得产品分类编号
            String productTypeId = productType.getProductTypeId();
            //设置产品分类编号
            adjustPriceInfo.setTypeId(productTypeId);
            //设置自定义主键
            adjustPriceInfo.setId(id);

            //保存分销价格信息
            boolean save = adjustPriceInfoService.save(adjustPriceInfo);
            if (save) {
                //根据token获得业务员信息
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("新增价格调整成功")
                        .put("list", id)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("新增价格调整失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除分销价格
     *
     * @param id
     * @return
     */
    @SystemLog(value = "删除分销价格")
    @PostMapping("/deleteAdjustPrice")
    @ApiOperation(value = "删除分销价格")
    @Transactional
    public CommonResult deleteAdjustPrice(String id, String token) {
        boolean flag = permissionsService.getPermissionList(token, "28");
        if (flag) {
            boolean remove = adjustPriceInfoService.removeById(id);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除分销价格成功")
                        .put("list", id)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除分销价格失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 更新分销价格
     *
     * @param adjustPriceInfoVo
     * @param token
     * @return
     */
    @SystemLog(value = "更新分销价格")
    @PostMapping("/updateAdjustPrice")
    @ApiOperation(value = "更新分销价格")
    @Transactional
    public CommonResult updateAdjustPrice(@RequestBody(required = false) AdjustPriceInfoVo adjustPriceInfoVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "28");
        if (flag) {

            AdjustPriceInfo adjustPriceInfo = new AdjustPriceInfo();
            BeanUtils.copyProperties(adjustPriceInfoVo, adjustPriceInfo);
            //获取产品分类
            //获取产品一级分类
            String productTypeOne = adjustPriceInfoVo.getProductTypeOne();
            //获取产品二级分类
            String productTypeTwo = adjustPriceInfoVo.getProductTypeTwo();
            //获取产品三级分类
            String productTypeThree = adjustPriceInfoVo.getProductTypeThree();
            //获取产品四级分类
            String productTypeFour = adjustPriceInfoVo.getProductTypeFour();
            //获取产品五级分类
            String productTypeFive = adjustPriceInfoVo.getProductTypeFive();
            //获取产品六级分类
            String productTypeSix = adjustPriceInfoVo.getProductTypeSix();
            //处理产品分类数据 通过产品分类查询产品分类编号
            QueryWrapper<ProductType> productTypeQueryWrapper = new QueryWrapper<>();
            if (!StringUtils.isEmpty(productTypeOne)) {
                productTypeQueryWrapper.and((wrapper) -> {
                    wrapper.eq("product_type_one", productTypeOne);
                    if (!StringUtils.isEmpty(productTypeTwo)) {
                        wrapper.eq("product_type_two", productTypeTwo);
                        if (!StringUtils.isEmpty(productTypeThree)) {
                            wrapper.eq("product_type_three", productTypeThree);
                            if (!StringUtils.isEmpty(productTypeFour)) {
                                wrapper.eq("product_type_four", productTypeFour);
                                if (!StringUtils.isEmpty(productTypeFive)) {
                                    wrapper.eq("product_type_five", productTypeFive);
                                    if (!StringUtils.isEmpty(productTypeSix)) {
                                        wrapper.eq("product_type_six", productTypeSix);
                                    }
                                }
                            }
                        }
                    }
                });
            }
            ProductType productType = productTypeService.getOne(productTypeQueryWrapper);
            //获得产品分类编号
            String productTypeId = productType.getProductTypeId();

            //设置主键编号
            String adjustPriceId = adjustPriceInfoVo.getAdjustPriceId();


            //设置主键与分类号
            adjustPriceInfo.setId(adjustPriceId);
            adjustPriceInfo.setTypeId(productTypeId);
            boolean update = adjustPriceInfoService.updateById(adjustPriceInfo);
            if (update) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("更新分销价格成功")
                        .put("list", adjustPriceId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("更新分销价格失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 查询分销价格列表
     *
     * @return
     */
    @GetMapping("/getAdjustPriceList")
    @ApiOperation(value = "查询分销价格列表")
    public CommonResult getAdjustPriceList() {
        List<AdjustPriceInfo> list = adjustPriceInfoService.list();
        List<AdjustSearchVo> adjustSearchVos = list.stream().map((adjust) -> {
            AdjustSearchVo adjustSearchVo = new AdjustSearchVo();
            //属性对拷
            BeanUtils.copyProperties(adjust, adjustSearchVo);

            //根据产品分类编号查询产品分类信息
            String typeId = adjust.getTypeId();
            ProductType productType = productTypeService.getById(typeId);
            //获取产品分类
            //获取产品一级分类
            String productTypeOne = productType.getProductTypeOne();
            //获取产品二级分类
            String productTypeTwo = productType.getProductTypeTwo();
            //获取产品三级分类
            String productTypeThree = productType.getProductTypeThree();
            //获取产品四级分类
            String productTypeFour = productType.getProductTypeFour();
            //获取产品五级分类
            String productTypeFive = productType.getProductTypeFive();
            //获取产品六级分类
            String productTypeSix = productType.getProductTypeSix();
            if (!StringUtils.isEmpty(productTypeOne)) {
                adjustSearchVo.setProductTypeOne(productTypeOne);
            }
            if (!StringUtils.isEmpty(productTypeTwo)) {
                adjustSearchVo.setProductTypeTwo(productTypeTwo);
            }
            if (!StringUtils.isEmpty(productTypeThree)) {
                adjustSearchVo.setProductTypeThree(productTypeThree);
            }
            if (!StringUtils.isEmpty(productTypeFour)) {
                adjustSearchVo.setProductTypeFour(productTypeFour);
            }
            if (!StringUtils.isEmpty(productTypeFive)) {
                adjustSearchVo.setProductTypeFive(productTypeFive);
            }
            if (!StringUtils.isEmpty(productTypeSix)) {
                adjustSearchVo.setProductTypeSix(productTypeSix);
            }
            return adjustSearchVo;
        }).collect(Collectors.toList());
        if (list.size() > 0) {
            return CommonResult.success("查询分销价格列表成功").put("adjustList", adjustSearchVos);
        } else {
            return CommonResult.success("查询分销价格列表失败");
        }
    }

    /**
     * 根据编号获取分销价格详情信息
     *
     * @param id
     * @return
     */
    @GetMapping("/getAdjustPriceById")
    @ApiOperation(value = "根据编号获取分销价格详情信息")
    public CommonResult getAdjustPriceById(String id) {
        AdjustPriceInfo adjustPriceInfo = adjustPriceInfoService.getById(id);
        AdjustPriceInfoVo adjustPriceInfoVo = new AdjustPriceInfoVo();
        BeanUtils.copyProperties(adjustPriceInfo, adjustPriceInfoVo);
        //根据产品分类编号查询产品分类信息
        String typeId = adjustPriceInfo.getTypeId();
        ProductType productType = productTypeService.getById(typeId);
        //获取产品分类
        //获取产品一级分类
        String productTypeOne = productType.getProductTypeOne();
        //获取产品二级分类
        String productTypeTwo = productType.getProductTypeTwo();
        //获取产品三级分类
        String productTypeThree = productType.getProductTypeThree();
        //获取产品四级分类
        String productTypeFour = productType.getProductTypeFour();
        //获取产品五级分类
        String productTypeFive = productType.getProductTypeFive();
        //获取产品六级分类
        String productTypeSix = productType.getProductTypeSix();
        if (!StringUtils.isEmpty(productTypeOne)) {
            adjustPriceInfoVo.setProductTypeOne(productTypeOne);
        }
        if (!StringUtils.isEmpty(productTypeTwo)) {
            adjustPriceInfoVo.setProductTypeTwo(productTypeTwo);
        }
        if (!StringUtils.isEmpty(productTypeThree)) {
            adjustPriceInfoVo.setProductTypeThree(productTypeThree);
        }
        if (!StringUtils.isEmpty(productTypeFour)) {
            adjustPriceInfoVo.setProductTypeFour(productTypeFour);
        }
        if (!StringUtils.isEmpty(productTypeFive)) {
            adjustPriceInfoVo.setProductTypeFive(productTypeFive);
        }
        if (!StringUtils.isEmpty(productTypeSix)) {
            adjustPriceInfoVo.setProductTypeSix(productTypeSix);
        }
        return CommonResult.success("查询分销价格成功").put("adjustPrice", adjustPriceInfoVo);
    }
}

