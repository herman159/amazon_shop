package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.Tort;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.TortService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.TortVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@RestController
@RequestMapping("/shop/tort")
@Api(tags = "侵权词模块")
public class TortController {
    @Resource
    private TortService tortService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 新增侵权词
     *
     * @param tortVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增侵权词")
    @PostMapping("/saveTort")
    @ApiOperation(value = "新增侵权词")
    @Transactional
    public CommonResult saveTort(@RequestBody TortVo tortVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "20");
        if (flag) {
            Tort tort = new Tort();

            //自定义侵权词编号
            String replace = UUID.randomUUID().toString().replace("-", "");
            String tortId = replace + "tort";
            tort.setTortId(tortId);

            //属性对拷
            BeanUtils.copyProperties(tortVo, tort);
            //保存侵权词
            boolean save = tortService.save(tort);
            if (save) {
                //根据token查询用户信息
                String userRealName = usersService.getUserRealName(token);

                return CommonResult.success("新增侵权词成功")
                        .put("list", tortId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("新增侵权词失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 查询侵权词列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getTortList")
    @ApiOperation(value = "查询侵权词列表")
    public CommonResult getTortList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "20");
        if (flag) {
            List<Tort> tortList = tortService.list();
            List<TortVo> tortVoList = tortList.stream().map((tort) -> {
                TortVo tortVo = new TortVo();
                BeanUtils.copyProperties(tort, tortVo);
                return tortVo;
            }).collect(Collectors.toList());
            return CommonResult.success().put("tortVoList", tortVoList);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 侵权词替换
     *
     * @param tortVo
     * @return
     */
    @SystemLog(value = "侵权词替换")
    @PostMapping("/replaceTort")
    @ApiOperation(value = "侵权词替换")
    @Transactional
    public CommonResult replaceTort(@RequestBody TortVo tortVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "20");
        if (flag) {
            //根据侵权词查询侵权词编号
            QueryWrapper<Tort> tortQueryWrapper = new QueryWrapper<>();
            String tortWord = tortVo.getTortWord();
            tortQueryWrapper.eq("tort_word", tortWord);
            Tort tort = tortService.getOne(tortQueryWrapper);

            //属性对拷
            BeanUtils.copyProperties(tortVo, tort);
            //更新信息
            boolean update = tortService.updateById(tort);
            if (update) {
                String tortId = tort.getTortId();
                //根据token查询业务员姓名
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("侵权词替换成功")
                        .put("list", tortId)
                        .put("salesman", userRealName);
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

