package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.ExpressForwarder;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.ExpressForwarderService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/express-forwarder")
@Api(tags = "物流转运商模块")
public class ExpressForwarderController {
    @Resource
    private ExpressForwarderService expressForwarderService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 获取物流转运商列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getExpressForwardServiceList")
    @ApiOperation(value = "获取物流转运商列表")
    public CommonResult getExpressForwardServiceList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "50");
        if (flag) {
            List<ExpressForwarder> expressForwarders = expressForwarderService.list();
            if (expressForwarders.size() > 0) {
                return CommonResult.success("获取物流转运商列表成功")
                        .put("expressForwarders", expressForwarders);
            } else {
                return CommonResult.success("获取物流转运商列表失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 设置转运商利润
     *
     * @param qq
     * @param profit
     * @return
     */
    @SystemLog(value = "设置转运商利润")
    @PostMapping("/setProfit")
    @ApiOperation(value = "设置转运商利润")
    @Transactional
    public CommonResult setProfit(String qq, String profit, String token) {
        boolean flag = permissionsService.getPermissionList(token, "50");
        if (flag) {
            //通过qq编号查询物流转运商信息
            QueryWrapper<ExpressForwarder> expressForwarderQueryWrapper = new QueryWrapper<>();
            expressForwarderQueryWrapper.eq("qq", qq);
            ExpressForwarder expressForwarder = expressForwarderService.getOne(expressForwarderQueryWrapper);

            //设置利润
            double parseDouble = Double.parseDouble(profit);
            BigDecimal profitChange = BigDecimal.valueOf(parseDouble);
            expressForwarder.setProfit(profitChange);

            //更新转运商信息
            boolean update = expressForwarderService.updateById(expressForwarder);
            if (update) {
                //获取协同号
                String number = expressForwarder.getNumber();
                //获取业务员姓名
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("更新利润成功")
                        .put("list", number)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("设置利润失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

