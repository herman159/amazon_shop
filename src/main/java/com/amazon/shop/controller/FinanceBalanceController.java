package com.amazon.shop.controller;


import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.FinanceBalance;
import com.amazon.shop.entity.auth.Apartments;
import com.amazon.shop.entity.auth.Roles;
import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.entity.auth.UsersRoles;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.*;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@RestController
@RequestMapping("/shop/finance-balance")
@Api(tags = "智赢余额模块")
public class FinanceBalanceController {
    @Resource
    private FinanceBalanceService financeBalanceService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersService usersService;
    @Resource
    private RolesService rolesService;
    @Resource
    private UsersRolesService usersRolesService;
    @Resource
    private ApartmentsService apartmentsService;

    /**
     * 查询余额信息
     *
     * @param token
     * @return
     */
    @GetMapping("/getBalance")
    @ApiOperation(value = "查询余额信息")
    public CommonResult getBalance(String token) {
        boolean flag = permissionsService.getPermissionList(token, "56");
        if (flag) {
            //根据token查询用户信息
            QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
            usersQueryWrapper.eq("token", token);
            Users users = usersService.getOne(usersQueryWrapper);

            //根据用户编号查询角色信息
            String userId = users.getUserId();
            QueryWrapper<UsersRoles> usersRolesQueryWrapper = new QueryWrapper<>();
            usersRolesQueryWrapper.eq("user_id", userId);
            UsersRoles usersRoles = usersRolesService.getOne(usersRolesQueryWrapper);

            //根据角色编号查询公司名称
            String roleId = usersRoles.getRoleId();
            Roles roles = rolesService.getById(roleId);

            //根据公司名称查询公司编号
            String apartmentName = roles.getApartmentName();
            QueryWrapper<Apartments> apartmentsQueryWrapper = new QueryWrapper<>();
            apartmentsQueryWrapper.eq("apartment_name", apartmentName);
            Apartments apartments = apartmentsService.getOne(apartmentsQueryWrapper);

            //根据公司编号查询余额信息
            String apartmentId = apartments.getApartmentId();
            FinanceBalance financeBalance = financeBalanceService.getById(apartmentId);
            return CommonResult.success().put("financeBalance", financeBalance);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

