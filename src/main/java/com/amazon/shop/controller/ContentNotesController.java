package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.ContentNotesService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.ContentNotesSaveVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/content-notes")
@Api(tags = "备注模块")
public class ContentNotesController {
    @Resource
    private ContentNotesService contentNotesService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 给具体的订单添加备注
     *
     * @param
     * @return
     */
    @SystemLog(value = "添加备注")
    @PostMapping("/saveContentNotes")
    @ApiOperation(value = "给具体的订单添加备注")
    @Transactional
    public CommonResult saveContentNotes(@RequestBody ContentNotesSaveVo contentNotesSaveVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "36");
        if (flag) {
            String orderId = contentNotesSaveVo.getOrderId();
            contentNotesService.saveContentNotes(contentNotesSaveVo, token);
            String userRealName = usersService.getUserRealName(token);
            return CommonResult.success("添加备注成功")
                    .put("list", orderId)
                    .put("salesman", userRealName);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

