package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.Bank;
import com.amazon.shop.entity.auth.Apartments;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.ApartmentsService;
import com.amazon.shop.service.BankService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.BankSearchVo;
import com.amazon.shop.vo.BankVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-27
 */
@RestController
@RequestMapping("/shop/bank")
@Api(tags = "银行模块")
public class BankController {
    @Resource
    private BankService bankService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private ApartmentsService apartmentsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 获取银行账户列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getBankList")
    @ApiOperation(value = "获取银行账户列表")
    public CommonResult getBankList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "59");
        if (flag) {
            List<Bank> list = bankService.list();
            List<BankSearchVo> bankVoList = list.stream().map((bank) -> {
                BankSearchVo bankSearchVo = new BankSearchVo();
                //属性对拷
                BeanUtils.copyProperties(bank, bankSearchVo);

                //处理时间
                String gmtCreated = bank.getGmtCreated();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date addTime = null;
                try {
                    addTime = format.parse(gmtCreated);
                    bankSearchVo.setAddTime(addTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //根据公司编号查询公司名称
                String companyId = bank.getCompanyId();
                Apartments apartments = apartmentsService.getById(companyId);
                String apartmentName = apartments.getApartmentName();
                bankSearchVo.setCompany(apartmentName);

                return bankSearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success("查询银行账户列表成功").put("bankVoList", bankVoList);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 增加银行账户
     *
     * @param bankVo
     * @param token
     * @return
     */
    @SystemLog(value = "增加银行账户")
    @PostMapping("/saveBank")
    @ApiOperation(value = "增加银行账户")
    @Transactional
    public CommonResult saveBank(@RequestBody BankVo bankVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "59");
        if (flag) {
            Bank bank = new Bank();
            //属性对拷
            BeanUtils.copyProperties(bankVo, bank);

            //自定义银行编号
            String bankId = UUID.randomUUID().toString().replace("-", "");
            bank.setBankId(bankId);

            //处理时间
            Date addTime = bankVo.getAddTime();
            bank.setGmtCreated(addTime.toString());

            //根据公司名称查询公司编号
            String company = bankVo.getCompany();
            QueryWrapper<Apartments> apartmentsQueryWrapper = new QueryWrapper<>();
            apartmentsQueryWrapper.eq("apartment_name", company);
            Apartments apartments = apartmentsService.getOne(apartmentsQueryWrapper);
            String apartmentId = apartments.getApartmentId();
            bank.setCompanyId(apartmentId);

            //保存银行信息
            boolean save = bankService.save(bank);
            if (save) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("添加银行账户成功")
                        .put("list", bankId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("添加银行账户失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除银行账户
     *
     * @param bankId
     * @param token
     * @return
     */
    @SystemLog(value = "删除银行账户")
    @PostMapping("/removeBank")
    @ApiOperation(value = "删除银行账户")
    @Transactional
    public CommonResult removeBank(String bankId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "59");
        if (flag) {
            boolean remove = bankService.removeById(bankId);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除银行账户成功")
                        .put("list", bankId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除银行账户成功");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

