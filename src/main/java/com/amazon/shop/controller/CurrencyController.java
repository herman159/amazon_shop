package com.amazon.shop.controller;


import com.amazon.shop.entity.Currency;
import com.amazon.shop.service.CurrencyService;
import com.amazon.shop.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-15
 */
@RestController
@RequestMapping("/shop/currency")
@Api(tags = "币种模块")
public class CurrencyController {
    @Resource
    private CurrencyService currencyService;

    /**
     * 获取币种列表
     *
     * @return
     */
    @GetMapping("/currencyList")
    @ApiOperation(value = "获取币种列表")
    public CommonResult currencyList() {
        List<Currency> currencyList = currencyService.list();
        if (currencyList.size() > 0) {
            return CommonResult.success("获取币种列表成功").put("currencyList", currencyList);
        } else {
            return CommonResult.error("获取币种列表失败");
        }
    }

    /**
     * 批量保存币种
     *
     * @param currencies
     * @return
     */
    @PostMapping("/saveCurrencyBatch")
    @ApiOperation(value = "批量保存币种")
    public CommonResult saveCurrencyBatch(@RequestBody List<Currency> currencies) {
        boolean batch = currencyService.saveBatch(currencies);
        if (batch) {
            return CommonResult.success("保存币种信息成功");
        } else {
            return CommonResult.error("保存币种信息失败");
        }
    }
}

