package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.Supplier;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.SupplierService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.SupplierSearchVo;
import com.amazon.shop.vo.SupplierVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/shop/supplier")
@Api(tags = "供应商模块")
public class SupplierController {
    @Resource
    private SupplierService supplierService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 新增供应商信息
     *
     * @param supplierVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增供应商信息")
    @Transactional
    @PostMapping("/saveSupplier")
    @ApiOperation(value = "新增供应商信息")
    public CommonResult saveSupplier(@RequestBody SupplierVo supplierVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "15");
        if (flag) {
            //属性对拷
            Supplier supplier = new Supplier();
            BeanUtils.copyProperties(supplierVo, supplier);

            //自定义供应商编号
            String supplierId = UUID.randomUUID().toString().replace("-", "");
            supplier.setSupplierId(supplierId);

            //保存供应商信息
            boolean save = supplierService.save(supplier);
            if (save) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("新增供应商信息成功")
                        .put("list", supplierId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("新增供应商信息失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 查询供应商列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getSupplier")
    @ApiOperation(value = "查询供应商列表")
    public CommonResult getSupplier(String token) {
        boolean flag = permissionsService.getPermissionList(token, "15");
        if (flag) {
            //查询供应商列表
            List<Supplier> supplierList = supplierService.list();
            List<SupplierSearchVo> supplierSearchVos = supplierList.stream().map((supplier) -> {
                SupplierSearchVo supplierSearchVo = new SupplierSearchVo();
                BeanUtils.copyProperties(supplier, supplierSearchVo);
                return supplierSearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success("查询供应商列表成功").put("supplierSearchVos", supplierSearchVos);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除供应商信息
     *
     * @param supplierId
     * @param token
     * @return
     */
    @SystemLog(value = "删除供应商信息")
    @ApiOperation(value = "删除供应商信息")
    @PostMapping("/removeSupplier")
    @Transactional
    public CommonResult removeSupplier(String supplierId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "15");
        if (flag) {
            boolean remove = supplierService.removeById(supplierId);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除供应商成功")
                        .put("list", supplierId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除供应商失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

