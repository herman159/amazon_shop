package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.Codes;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.CodesService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.CodeSaveVo;
import com.amazon.shop.vo.CodesVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-24
 */
@RestController
@RequestMapping("/shop/codes")
@Api(tags = "编码模块")
public class CodesController {
    @Resource
    private CodesService codesService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 获取编码列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getCodesList")
    @ApiOperation(value = "获取编码列表")
    public CommonResult getCodesList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "29");
        if (flag) {
            List<Codes> codesList = codesService.list();
            if (codesList.size() > 0) {
                List<CodesVo> codesVoList = codesList.stream().map((code) -> {
                    CodesVo codesVo = new CodesVo();
                    BeanUtils.copyProperties(code, codesVo);
                    //设置时间
                    String gmtCreated = code.getGmtCreated();
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date parse = null;
                    try {
                        parse = format.parse(gmtCreated);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    codesVo.setAddTime(parse);
                    return codesVo;
                }).collect(Collectors.toList());
                return CommonResult.success("查询编码列表成功").put("codesList", codesVoList);
            } else {
                return CommonResult.success("查询编码列表失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除编码
     *
     * @param token
     * @param code
     * @return
     */
    @SystemLog(value = "删除编码")
    @PostMapping("/deleteCodes")
    @ApiOperation(value = "删除编码")
    @Transactional
    public CommonResult deleteCodes(String token, String code) {
        boolean flag = permissionsService.getPermissionList(token, "29");
        if (flag) {
            boolean remove = codesService.removeById(code);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除编码成功")
                        .put("list", code)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除编码失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 批量保存编码
     *
     * @param codesVoList
     * @param token
     * @return
     */
    @SystemLog(value = "批量保存编码")
    @PostMapping("/saveCodesBatch")
    @ApiOperation(value = "批量保存编码")
    @Transactional
    public CommonResult saveCodesBatch(@RequestBody List<CodeSaveVo> codesVoList, String token) {
        boolean flag = permissionsService.getPermissionList(token, "29");
        if (flag) {

            List<Codes> codesList = codesVoList.stream().map((codeVo) -> {
                String replace = UUID.randomUUID().toString().replace("-", "");
                String code = replace + "code";
                Codes codes = new Codes();
                BeanUtils.copyProperties(codeVo, codes);

                //设置时间
                Date addTime = codeVo.getAddTime();
                codes.setGmtCreated(addTime.toString());

                //设置自定义编码
                codes.setCode(code);
                return codes;
            }).collect(Collectors.toList());
            boolean batch = codesService.saveBatch(codesList);
            if (batch) {

                //查询编号
                List<Codes> list = codesService.list();
                List<String> collect = list.stream().map((item) -> {
                    String code = item.getCode();
                    return code;
                }).collect(Collectors.toList());

                //查询业务员
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("批量保存编码成功")
                        .put("list", collect)
                        .put("salesman", userRealName);
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

