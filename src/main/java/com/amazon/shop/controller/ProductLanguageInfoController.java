package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.Country;
import com.amazon.shop.entity.ProductLanguageInfo;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.CountryService;
import com.amazon.shop.service.ProductLanguageInfoService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.ProductLanguageInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-16
 */
@RestController
@RequestMapping("/shop/product-language-info")
@Api(tags = "产品翻译信息模块")
public class ProductLanguageInfoController {
    @Resource
    private ProductLanguageInfoService productLanguageInfoService;
    @Resource
    private CountryService countryService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 保存产品翻译信息
     *
     * @param productLanguageInfoVo
     * @return
     */
    @SystemLog(value = "保存产品翻译信息")
    @PostMapping("/saveProductInfo")
    @ApiOperation(value = "保存产品翻译信息")
    @Transactional
    public CommonResult saveProductInfo(@RequestBody ProductLanguageInfoVo productLanguageInfoVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "10");
        if (flag) {
            ProductLanguageInfo productLanguageInfo = new ProductLanguageInfo();
            String countryId = productLanguageInfoVo.getCountryId();

            //通过国家编号查询国家信息
            Country country = countryService.getById(countryId);
            String name = country.getName();
            productLanguageInfo.setCountry(name);

            //保存信息
            boolean save = productLanguageInfoService.save(productLanguageInfo);
            if (save) {
                String productId = productLanguageInfoVo.getProductId();
                String userRealName = usersService.getUserRealName(token);

                return CommonResult.success("保存产品翻译信息成功")
                        .put("list", productId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("保存产品翻译信息失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

