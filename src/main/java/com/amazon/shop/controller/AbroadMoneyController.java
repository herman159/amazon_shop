package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.AbroadMoney;
import com.amazon.shop.entity.Country;
import com.amazon.shop.entity.Currency;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.AbroadMoneyService;
import com.amazon.shop.service.CountryService;
import com.amazon.shop.service.CurrencyService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.AbroadMoneySearchVo;
import com.amazon.shop.vo.AbroadMoneyVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
@RestController
@RequestMapping("/shop/abroad-money")
@Api(tags = "国外汇款模块")
public class AbroadMoneyController {
    @Resource
    private AbroadMoneyService abroadMoneyService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private CurrencyService currencyService;
    @Resource
    private CountryService countryService;

    /**
     * 新增国外汇款信息
     *
     * @param abroadMoneyVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增国外汇款信息")
    @PostMapping("/saveAbroadMoney")
    @ApiOperation(value = "新增国外汇款信息")
    @Transactional
    public CommonResult saveAbroadMoney(@RequestBody AbroadMoneyVo abroadMoneyVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            AbroadMoney abroadMoney = new AbroadMoney();
            //属性对拷
            BeanUtils.copyProperties(abroadMoneyVo, abroadMoney);

            //设置自定义编号
            String abroadMoneyId = UUID.randomUUID().toString().replace("-", "");
            abroadMoney.setAbroadMoneyId(abroadMoneyId);

            //根据币种名称查询币种编号
            String currencyName = abroadMoneyVo.getCurrency();
            QueryWrapper<Currency> currencyQueryWrapper = new QueryWrapper<>();
            currencyQueryWrapper.eq("currency_name", currencyName);
            Currency currency = currencyService.getOne(currencyQueryWrapper);
            String currencyId = currency.getCurrencyId();
            //设置币种编号
            abroadMoney.setCurrencyId(currencyId);

            //根据国家名称查询国家编号
            String countryName = abroadMoneyVo.getCountry();
            QueryWrapper<Country> countryQueryWrapper = new QueryWrapper<>();
            countryQueryWrapper.eq("name", countryName);
            Country country = countryService.getOne(countryQueryWrapper);
            String id = country.getId();
            //设置国家编号
            abroadMoney.setCountryId(id);

            //设置录入人员
            String userRealName = usersService.getUserRealName(token);
            abroadMoney.setUsername(userRealName);

            //保存国外汇款信息
            boolean save = abroadMoneyService.save(abroadMoney);
            if (save) {
                return CommonResult.success("新增国外汇款信息成功")
                        .put("list", abroadMoneyId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("新增国外汇款信息失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 获取国外汇款信息列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getAbroadMoneyList")
    @ApiOperation(value = "获取国外汇款信息列表")
    public CommonResult getAbroadMoneyList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            //查询列表
            List<AbroadMoney> abroadMoneyList = abroadMoneyService.list();

            List<AbroadMoneySearchVo> abroadMoneySearchVos = abroadMoneyList.stream().map((abroadMoney) -> {
                AbroadMoneySearchVo abroadMoneySearchVo = new AbroadMoneySearchVo();
                //属性对拷
                BeanUtils.copyProperties(abroadMoney, abroadMoneySearchVo);

                //根据币种编号查询币种名称
                String currencyId = abroadMoney.getCurrencyId();
                Currency currency = currencyService.getById(currencyId);
                //设置币种
                String currencyName = currency.getCurrencyName();
                abroadMoneySearchVo.setCurrency(currencyName);

                //根据国家编号查询国家名称
                String countryId = abroadMoney.getCountryId();
                Country country = countryService.getById(countryId);
                //设置国家
                String name = country.getName();
                abroadMoneySearchVo.setCountry(name);

                //设置添加时间
                String gmtCreated = abroadMoney.getGmtCreated();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date addTime = null;
                try {
                    addTime = dateFormat.parse(gmtCreated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                abroadMoneySearchVo.setAddTime(addTime);
                return abroadMoneySearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success("获取国外汇款信息列表成功")
                    .put("abroadMoneySearchVos", abroadMoneySearchVos);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除国外汇款信息
     *
     * @param token
     * @param id
     * @return
     */
    @SystemLog(value = "删除国外汇款信息")
    @PostMapping("/removeAbroadMoney")
    @ApiOperation(value = "删除国外汇款信息")
    @Transactional
    public CommonResult removeAbroadMoney(String token, String id) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            //删除国外汇款信息
            boolean remove = abroadMoneyService.removeById(id);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除国外汇款信息成功")
                        .put("list", id)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除国外汇款信息成功");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

