package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.BuyerInfo;
import com.amazon.shop.entity.Orders;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.BuyerInfoService;
import com.amazon.shop.service.OrderSimpleService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.BuyerInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/buyer-info")
@Api(tags = "买家模块")
public class BuyerInfoController {
    @Resource
    private BuyerInfoService buyerInfoService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private OrderSimpleService orderSimpleService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 查询买家信息
     *
     * @param orderId
     * @param token
     * @return
     */
    @GetMapping("/getBuyerInfo")
    @ApiOperation(value = "查询买家信息")
    public CommonResult getBuyerInfo(String orderId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "36");
        if (flag) {
            //根据订单编号获取买家编号
            Orders orders = orderSimpleService.getById(orderId);
            String buyerId = orders.getBuyerId();

            //根据买家编号查询买家信息
            BuyerInfo buyerInfo = buyerInfoService.getById(buyerId);
            BuyerInfoVo buyerInfoVo = new BuyerInfoVo();

            //对应属性对拷
            BeanUtils.copyProperties(buyerInfo, buyerInfoVo);
            return CommonResult.success("查询买家信息成功").put("buyerInfo", buyerInfoVo);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 设置买家信息
     *
     * @param orderId
     * @param token
     * @return
     */
    @SystemLog(value = "设置买家信息")
    @PostMapping("/setBuyerInfo")
    @ApiOperation(value = "设置买家信息")
    @Transactional
    public CommonResult setBuyerInfo(@RequestBody BuyerInfoVo buyerInfoVo, String orderId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "36");
        if (flag) {
            Orders orders = orderSimpleService.getById(orderId);

            //设置自定义买家编号
            String buyerId = orders.getOrderNumber().replace("order", "buyer");
            BuyerInfo buyerInfo = new BuyerInfo();
            buyerInfo.setId(buyerId);

            //设置销售单号
            String amazonOrderId = orders.getAmazonOrderId();
            buyerInfo.setAmazonOrderId(amazonOrderId);

            //对应属性对拷
            BeanUtils.copyProperties(buyerInfoVo, buyerInfo);

            //订单设置买家编号
            orders.setBuyerId(buyerId);
            orderSimpleService.updateById(orders);

            //保存买家信息
            buyerInfoService.save(buyerInfo);

            //根据token查询业务员名字
            String userRealName = usersService.getUserRealName(token);
            return CommonResult.success("保存买家信息成功")
                    .put("list", orderId)
                    .put("salesman", userRealName);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

