package com.amazon.shop.controller;


import com.amazon.shop.entity.Country;
import com.amazon.shop.service.CountryService;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.CountryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/country")
@Api(tags = "国家模块")
public class CountryController {
    @Resource
    private CountryService countryService;

    /**
     * 批量保存国家信息
     *
     * @param countries
     * @return
     */
    @PostMapping("/saveCountryBatch")
    @ApiOperation(value = "批量保存国家信息")
    public CommonResult saveCountryBatch(@RequestBody List<CountryVo> countries) {
        List<Country> countryList = countries.stream().map((vo) -> {
            Country country = new Country();
            BeanUtils.copyProperties(vo, country);
            return country;
        }).collect(Collectors.toList());
        boolean batch = countryService.saveBatch(countryList);
        if (batch) {
            return CommonResult.success("国家信息保存成功");
        } else {
            return CommonResult.error("国家信息保存失败");
        }
    }
}

