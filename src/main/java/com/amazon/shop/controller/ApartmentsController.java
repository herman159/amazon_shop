package com.amazon.shop.controller;


import com.amazon.shop.entity.auth.Apartments;
import com.amazon.shop.entity.auth.Roles;
import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.entity.auth.UsersRoles;
import com.amazon.shop.service.ApartmentsService;
import com.amazon.shop.service.RolesService;
import com.amazon.shop.service.UsersRolesService;
import com.amazon.shop.service.UsersService;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.ApartmentsSearchVo;
import com.amazon.shop.vo.ApartmentsVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-29
 */
@RestController
@RequestMapping("/shop/apartments")
@Api(tags = "部门模块")
public class ApartmentsController {
    @Resource
    private ApartmentsService apartmentsService;
    @Resource
    private RolesService rolesService;
    @Resource
    private UsersRolesService usersRolesService;
    @Resource
    private UsersService usersService;

    /**
     * 获取部门列表信息
     *
     * @return
     */
    @GetMapping("/getApartment")
    @ApiOperation(value = "获取部门列表信息")
    public CommonResult getApartment() {
        List<Apartments> apartmentsList = apartmentsService.list();

        List<ApartmentsSearchVo> searchVos = apartmentsList.stream().map((apartment) -> {
            ApartmentsSearchVo apartmentsSearchVo = new ApartmentsSearchVo();
            //属性对拷
            BeanUtils.copyProperties(apartment, apartmentsSearchVo);

            //根据部门名称查询部门所属人员
            String apartmentName = apartment.getApartmentName();
            QueryWrapper<Roles> rolesQueryWrapper = new QueryWrapper<>();
            rolesQueryWrapper.eq("apartment_name", apartmentName);
            List<Roles> roles = rolesService.list(rolesQueryWrapper);

            //获取角色编号
            List<String> idList = roles.stream().map((role) -> {
                String roleId = role.getRoleId();
                return roleId;
            }).collect(Collectors.toList());

            List<String> ids = new ArrayList<>();

            //根据角色编号查询用户列表
            List<List<String>> lists = idList.stream().map((id) -> {
                QueryWrapper<UsersRoles> usersRolesQueryWrapper = new QueryWrapper<>();
                usersRolesQueryWrapper.eq("role_id", id);
                List<UsersRoles> usersRolesList = usersRolesService.list(usersRolesQueryWrapper);
                List<String> collect = usersRolesList.stream().map((userRoles) -> {
                    String userId = userRoles.getUserId();
                    return userId;
                }).collect(Collectors.toList());
                ids.addAll(collect);
                return collect;
            }).collect(Collectors.toList());

            //通过用户编号列表查询用户列表
            if (!ids.isEmpty()) {
                List<Users> users = usersService.listByIds(ids);

                //通过用户列表获得用户姓名列表
                List<String> userNameList =
                        users.stream().map(Users::getUserName).collect(Collectors.toList());

                //设置员工列表
                apartmentsSearchVo.setStaffs(userNameList);
            }

            return apartmentsSearchVo;
        }).collect(Collectors.toList());


        return CommonResult.success().put("searchVos", searchVos);
    }

    /**
     * 删除部门信息
     *
     * @param id
     * @return
     */
    @PostMapping("/removeApartment")
    @ApiOperation(value = "删除部门信息")
    public CommonResult removeApartment(String id) {
        boolean remove = apartmentsService.removeById(id);
        if (remove) {
            return CommonResult.success("删除部门信息成功");
        } else {
            return CommonResult.error("删除部门信息失败");
        }
    }

    /**
     * 新增部门信息
     *
     * @param apartmentsVo
     * @return
     */
    @PostMapping("/saveApartment")
    @ApiOperation(value = "新增部门信息")
    public CommonResult saveApartment(@RequestBody ApartmentsVo apartmentsVo) {
        Apartments apartments = new Apartments();
        //属性对拷
        BeanUtils.copyProperties(apartmentsVo, apartments);

        //设置自定义部门编号
        String id = UUID.randomUUID().toString().replace("-", "");
        apartments.setApartmentId(id);

        //保存部门信息
        boolean save = apartmentsService.save(apartments);
        if (save) {
            return CommonResult.success("新增部门信息成功");
        } else {
            return CommonResult.error("新增部门信息失败");
        }
    }
}

