package com.amazon.shop.controller;


import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.service.AmazonAuthorityService;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.AmazonAuthorityVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/amazon-authority")
@Api(tags = "开发者权限模块")
public class AmazonAuthorityController {
    @Resource
    private AmazonAuthorityService amazonAuthorityService;

    /**
     * 新增亚马逊开发者权限
     *
     * @return
     */
    @PostMapping("/saveAmazonAuthority")
    @ApiOperation(value = "新增亚马逊开发者权限")
    public CommonResult saveAmazonAuthority(@RequestBody AmazonAuthorityVo amazonAuthorityVo) {
        AmazonAuthority amazonAuthority = new AmazonAuthority();
        BeanUtils.copyProperties(amazonAuthorityVo, amazonAuthority);
        boolean save = amazonAuthorityService.saveOrUpdate(amazonAuthority);
        if (save) {
            return CommonResult.success("新增亚马逊开发者权限成功");
        } else {
            return CommonResult.error("新增亚马逊开发者权限失败");
        }
    }
}

