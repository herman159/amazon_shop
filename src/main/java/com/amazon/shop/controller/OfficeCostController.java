package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.OfficeCost;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.OfficeCostService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.OfficeCostSearchVo;
import com.amazon.shop.vo.OfficeCostVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-02
 */
@RestController
@RequestMapping("/shop/office-cost")
@Api(tags = "办公费用模块")
public class OfficeCostController {
    @Resource
    private OfficeCostService officeCostService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private PermissionsServiceImpl permissionsService;

    /**
     * 新增办公费用
     *
     * @param officeCostVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增办公费用")
    @PostMapping("/saveOfficeCost")
    @ApiOperation(value = "新增办公费用")
    @Transactional
    public CommonResult saveOfficeCost(@RequestBody OfficeCostVo officeCostVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            //属性对拷
            OfficeCost officeCost = new OfficeCost();
            BeanUtils.copyProperties(officeCostVo, officeCost);

            //自定义办公费用编号
            String officeCostId = UUID.randomUUID().toString().replace("-", "");
            officeCost.setOfficeCostId(officeCostId);

            boolean save = officeCostService.save(officeCost);
            if (save) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("新增办公费用成功")
                        .put("list", officeCostId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("新增办公费用失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 查询办公费用列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getOfficeCost")
    @ApiOperation(value = "查询办公费用列表")
    public CommonResult getOfficeCost(String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            List<OfficeCost> list = officeCostService.list();
            List<OfficeCostSearchVo> officeCostSearchVoList = list.stream().map((officeCost) -> {
                //属性对拷
                OfficeCostSearchVo officeCostSearchVo = new OfficeCostSearchVo();
                BeanUtils.copyProperties(officeCost, officeCostSearchVo);

                //处理时间
                String gmtModified = officeCost.getGmtModified();
                String gmtCreated = officeCost.getGmtCreated();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                Date addTime = null;
                try {
                    addTime = dateFormat.parse(gmtModified);
                    date = dateFormat.parse(gmtCreated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                officeCostSearchVo.setAddTime(addTime);
                officeCostSearchVo.setDate(date);

                return officeCostSearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success("查询办公费用列表成功")
                    .put("officeCostSearchVoList", officeCostSearchVoList);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除办公费用信息
     *
     * @param officeCostId
     * @param token
     * @return
     */
    @SystemLog(value = "删除办公费用信息")
    @PostMapping("/removeOfficeCost")
    @ApiOperation(value = "删除办公费用信息")
    @Transactional
    public CommonResult removeOfficeCost(String officeCostId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            boolean remove = officeCostService.removeById(officeCostId);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除办公费用信息成功")
                        .put("list", officeCostId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除办公费用信息失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

