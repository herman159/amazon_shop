package com.amazon.shop.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/shop/commodity")
public class CommodityController {

}

