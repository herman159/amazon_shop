package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.ProductType;
import com.amazon.shop.entity.TitleWord;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.ProductTypeService;
import com.amazon.shop.service.TitleWordService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.TitleWordSearchVo;
import com.amazon.shop.vo.TitleWordVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/shop/title-word")
@Api(tags = "标题模板模块")
public class TitleWordController {
    @Resource
    private TitleWordService titleWordService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private ProductTypeService productTypeService;

    /**
     * 新增标题模板
     *
     * @param titleWordVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增标题模板")
    @PostMapping("/saveTitle")
    @ApiOperation(value = "新增标题模板")
    @Transactional
    public CommonResult saveTitle(@RequestBody TitleWordVo titleWordVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "23");
        if (flag) {
            TitleWord titleWord = new TitleWord();
            BeanUtils.copyProperties(titleWordVo, titleWord);

            //处理分类字符串
            String type = titleWordVo.getType();
            String typeOne = null;
            String typeTwo = null;
            String typeThree = null;
            String typeFour = null;
            String typeFive = null;
            String typeSix = null;
            String[] split = type.split("/");
            try {
                if (!StringUtils.isEmpty(split[0])) {
                    typeOne = split[0];
                    if (!StringUtils.isEmpty(split[1])) {
                        typeTwo = split[1];
                        if (!StringUtils.isEmpty(split[2])) {
                            typeThree = split[2];
                            if (!StringUtils.isEmpty(split[3])) {
                                typeFour = split[3];
                                if (!StringUtils.isEmpty(split[4])) {
                                    typeFive = split[4];
                                    if (!StringUtils.isEmpty(split[5])) {
                                        typeSix = split[5];
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //条件查询分类编号
            QueryWrapper<ProductType> productTypeQueryWrapper = new QueryWrapper<>();
            if (!StringUtils.isEmpty(typeOne)) {
                productTypeQueryWrapper.eq("product_type_one", typeOne);
                if (!StringUtils.isEmpty(typeTwo)) {
                    productTypeQueryWrapper.eq("product_type_two", typeTwo);
                    if (!StringUtils.isEmpty(typeThree)) {
                        productTypeQueryWrapper.eq("product_type_three", typeThree);
                        if (!StringUtils.isEmpty(typeFour)) {
                            productTypeQueryWrapper.eq("product_type_four", typeFour);
                            if (!StringUtils.isEmpty(typeFive)) {
                                productTypeQueryWrapper.eq("product_type_five", typeFive);
                                if (!StringUtils.isEmpty(typeSix)) {
                                    productTypeQueryWrapper.eq("product_type_six", typeSix);
                                }
                            }
                        }
                    }
                }
            }
            ProductType productType = productTypeService.getOne(productTypeQueryWrapper);

            //设置产品分类编号
            String productTypeId = productType.getProductTypeId();
            titleWord.setTypeId(productTypeId);

            //自定义标题编号
            String titleWordId = UUID.randomUUID().toString().replace("-", "");
            titleWord.setTitleWordId(titleWordId);

            //保存标题信息
            boolean save = titleWordService.save(titleWord);
            if (save) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("新增标题模板成功")
                        .put("list", titleWordId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("新增标题模板失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 获取标题模板列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getTitleList")
    @ApiOperation(value = "获取标题模板列表")
    public CommonResult getTitleList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "23");
        if (flag) {
            List<TitleWord> titleWordList = titleWordService.list();

            List<TitleWordSearchVo> titleWordSearchVos = titleWordList.stream().map((titleWord) -> {
                TitleWordSearchVo titleWordSearchVo = new TitleWordSearchVo();
                //属性对拷
                BeanUtils.copyProperties(titleWord, titleWordSearchVo);

                String typeId = titleWord.getTypeId();
                ProductType productType = productTypeService.getById(typeId);

                //获取分类
                String productTypeOne = productType.getProductTypeOne();
                String productTypeTwo = productType.getProductTypeTwo();
                String productTypeThree = productType.getProductTypeThree();
                String productTypeFour = productType.getProductTypeFour();
                String productTypeFive = productType.getProductTypeFive();
                String productTypeSix = productType.getProductTypeSix();
                String type = "";

                //拼接种类
                if (!StringUtils.isEmpty(productTypeOne)) {
                    type = type.concat(productTypeOne).concat("/");
                    if (!StringUtils.isEmpty(productTypeTwo)) {
                        type = type.concat(productTypeTwo).concat("/");
                        if (!StringUtils.isEmpty(productTypeThree)) {
                            type = type.concat(productTypeThree).concat("/");
                            if (!StringUtils.isEmpty(productTypeFour)) {
                                type = type.concat(productTypeFour).concat("/");
                                if (!StringUtils.isEmpty(productTypeFive)) {
                                    type = type.concat(productTypeFive).concat("/");
                                    if (!StringUtils.isEmpty(productTypeSix)) {
                                        type = type.concat(productTypeSix);
                                    }
                                }
                            }
                        }
                    }
                }
                titleWordSearchVo.setType(type);

                //设置时间
                String gmtCreated = titleWord.getGmtCreated();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date addTime = null;
                try {
                    addTime = dateFormat.parse(gmtCreated);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                titleWordSearchVo.setAddTime(addTime);
                return titleWordSearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success("获取标题模板列表成功").put("titleWordSearchVos", titleWordSearchVos);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除产品标题信息
     *
     * @param titleId
     * @param token
     * @return
     */
    @SystemLog(value = "删除产品标题信息")
    @PostMapping("/removeTitle")
    @ApiOperation(value = "删除产品标题信息")
    @Transactional
    public CommonResult removeTitle(String titleId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "23");
        if (flag) {
            boolean remove = titleWordService.removeById(titleId);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除产品标题信息成功")
                        .put("list", titleId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除产品标题信息失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

