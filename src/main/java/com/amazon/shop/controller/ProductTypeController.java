package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.ProductType;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.ProductTypeService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.ProductTypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/product-type")
@Api(tags = "产品分类模块")
public class ProductTypeController {
    @Resource
    private ProductTypeService productTypeService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 新增产品分类
     *
     * @param productTypeVoList
     * @return
     */
    @SystemLog(value = "新增产品分类")
    @PostMapping("/saveProductType")
    @ApiOperation(value = "新增产品分类")
    @Transactional
    public CommonResult saveProductType(@RequestBody List<ProductTypeVo> productTypeVoList, String token) {
        boolean flag = permissionsService.getPermissionList(token, "12");
        if (flag) {
            List<ProductType> productTypeList = productTypeVoList.stream().map((productTypeVo) -> {
                ProductType productType = new ProductType();

                //设置自定义产品编号
                String replace = UUID.randomUUID().toString().replace("-", "");
                String productTypeId = replace + "type";
                productType.setProductTypeId(productTypeId);

                if (!StringUtils.isEmpty(productTypeVo.getProductTypeOne())) {
                    productType.setProductTypeOne(productTypeVo.getProductTypeOne());
                }
                if (!StringUtils.isEmpty(productTypeVo.getProductTypeTwo())) {
                    productType.setProductTypeTwo(productTypeVo.getProductTypeTwo());
                }
                if (!StringUtils.isEmpty(productTypeVo.getProductTypeThree())) {
                    productType.setProductTypeThree(productTypeVo.getProductTypeThree());
                }
                if (!StringUtils.isEmpty(productTypeVo.getProductTypeFour())) {
                    productType.setProductTypeFour(productTypeVo.getProductTypeFour());
                }
                if (!StringUtils.isEmpty(productTypeVo.getProductTypeFive())) {
                    productType.setProductTypeFive(productTypeVo.getProductTypeFive());
                }
                if (!StringUtils.isEmpty(productTypeVo.getProductTypeSix())) {
                    productType.setProductTypeSix(productTypeVo.getProductTypeSix());
                }
                return productType;
            }).collect(Collectors.toList());

            //批量保存产品分类信息
            boolean saveBatch = productTypeService.saveBatch(productTypeList);

            if (saveBatch) {
                //查询保存的编号
                List<String> collect = productTypeList.stream().map((type) -> {
                    String productTypeId = type.getProductTypeId();
                    return productTypeId;
                }).collect(Collectors.toList());

                String userRealName = usersService.getUserRealName(token);

                return CommonResult.success("保存产品分类成功")
                        .put("list", collect)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("保存产品分类失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 获取产品分类列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getTypeList")
    @ApiOperation(value = "获取产品分类列表")
    public CommonResult getTypeList(String token) {
        boolean flag = permissionsService.getPermissionList(token, "12");
        if (flag) {
            List<ProductType> productTypeList = productTypeService.list();

            return CommonResult.success().put("productTypeList",productTypeList);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

