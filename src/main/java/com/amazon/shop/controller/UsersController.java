package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.auth.Users;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/shop/users")
@Api(tags = "用户模块")
public class UsersController {
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 用户登录
     *
     * @return
     */
    @SystemLog(value = "用户登录")
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public CommonResult login(@RequestBody UserLoginVo userLoginVo) {
        UsersVo usersVoLogin = usersService.login(userLoginVo);
        String userName = usersVoLogin.getUserName();
        //获取用户登录token
        String token = usersVoLogin.getToken();

        //获取用户编号
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("user_name", userName);
        Users users = usersService.getOne(usersQueryWrapper);
        String userId = users.getUserId();

        //根据token获取业务员信息
        String userRealName = usersService.getUserRealName(token);
        return CommonResult.success("登录成功")
                .put("userInfo", usersVoLogin)
                .put("salesman", userRealName)
                .put("list", userId);
    }

    /**
     * 修改密码
     *
     * @param passwordVo
     * @return
     */
    @SystemLog(value = "修改密码")
    @PostMapping("/updatePassword")
    @ApiOperation(value = "修改密码")
    @Transactional
    public CommonResult updatePassword(@RequestBody UsersPasswordVo passwordVo, String token) {
        //更新用户信息
        usersService.updatePassword(passwordVo, token);

        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("token", token);
        Users users = usersService.getOne(usersQueryWrapper);
        String userId = users.getUserId();
        String userRealName = usersService.getUserRealName(token);

        return CommonResult.success("密码修改成功")
                .put("list", userId)
                .put("salesman", userRealName);
    }

    /**
     * 更新用户信息
     *
     * @param usersUpdateVo
     * @return
     */
    @SystemLog(value = "更新用户信息")
    @PostMapping("/updateUserInfo")
    @ApiOperation(value = "更新用户信息")
    @Transactional
    public CommonResult updateUserInfo(@RequestBody UsersUpdateVo usersUpdateVo, String token) {
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        usersQueryWrapper.eq("token", token);
        Users users = usersService.getOne(usersQueryWrapper);
        String userId = users.getUserId();
        if (users != null) {
            BeanUtils.copyProperties(usersUpdateVo, users);
            String voToken = users.getToken();

            QueryWrapper<Users> wrapper = new QueryWrapper<>();
            wrapper.eq("token", voToken);
            int count = usersService.count(wrapper);

            //判断token是否重复
            if (count > 0) {
                throw new CustomException(ReturnConstant.HAD_TOKEN_ERROR_CODE
                        , ReturnConstant.HAD_TOKEN_ERROR_MESSAGE);
            }

            //不重复则更新用户信息
            boolean update = usersService.saveOrUpdate(users);
            if (update) {
                String userRealName = usersService.getUserRealName(voToken);
                return CommonResult.success("更新用户信息成功")
                        .put("list", userId)
                        .put("salesman", userRealName);
            }
        }
        return CommonResult.error(ReturnConstant.UPDATE_USER_INFO_ERROR_CODE
                , ReturnConstant.UPDATE_USER_INFO_ERROR_MESSAGE);
    }

    /**
     * 查看用户信息
     *
     * @param token
     * @return
     */
    @GetMapping("/getUserInfo")
    @ApiOperation(value = "查看用户信息")
    public CommonResult getUserInfo(String token) {
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<>();
        //通过token查询用户信息
        usersQueryWrapper.eq("token", token);
        Users users = usersService.getOne(usersQueryWrapper);
        UsersGetVo usersGetVo = new UsersGetVo();
        //对应属性对拷
        BeanUtils.copyProperties(users, usersGetVo);

        return CommonResult.success().put("users", usersGetVo);
    }
}

