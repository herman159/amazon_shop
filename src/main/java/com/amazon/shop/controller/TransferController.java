package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.Transfer;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.TransferService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.TransferSearchVo;
import com.amazon.shop.vo.TransferVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-07-03
 */
@RestController
@RequestMapping("/shop/transfer")
@Api(tags = "转账充值模块")
public class TransferController {
    @Resource
    private TransferService transferService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private UsersServiceImpl usersService;

    /**
     * 新增转账充值信息
     *
     * @param transferVo
     * @param token
     * @return
     */
    @SystemLog(value = "新增转账充值信息")
    @PostMapping("/saveTransfer")
    @ApiOperation(value = "新增转账充值信息")
    @Transactional
    public CommonResult saveTransfer(@RequestBody TransferVo transferVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            //属性对拷
            Transfer transfer = new Transfer();
            BeanUtils.copyProperties(transferVo, transfer);

            //自定义编号
            String transferId = UUID.randomUUID().toString().replace("-", "");
            transfer.setTransferId(transferId);

            //保存转账充值信息
            boolean save = transferService.save(transfer);
            if (save) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("新增转账充值信息成功")
                        .put("list", transferId)
                        .put("salesman", userRealName);
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 获取转账充值列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getTransfer")
    @ApiOperation(value = "获取转账充值列表")
    public CommonResult getTransfer(String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            List<Transfer> list = transferService.list();
            list.stream().map((transfer) -> {
                //属性对拷
                TransferSearchVo transferSearchVo = new TransferSearchVo();
                BeanUtils.copyProperties(transfer, transferSearchVo);

                //处理时间
                String gmtCreated = transfer.getGmtCreated();
                String modified = transfer.getGmtModified();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                Date addTime = null;
                try {
                    date = dateFormat.parse(gmtCreated);
                    addTime = dateFormat.parse(modified);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                transferSearchVo.setAddTime(addTime);
                transferSearchVo.setDate(date);

                return transferSearchVo;
            }).collect(Collectors.toList());

            return CommonResult.success();
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除转账充值列表
     *
     * @param transferId
     * @param token
     * @return
     */
    @PostMapping("/removeTransfer")
    @ApiOperation(value = "删除转账充值列表")
    @SystemLog(value = "删除转账充值列表")
    @Transactional
    public CommonResult removeTransfer(String transferId, String token) {
        boolean flag = permissionsService.getPermissionList(token, "55");
        if (flag) {
            boolean remove = transferService.removeById(transferId);
            if (remove) {
                String userRealName = usersService.getUserRealName(token);
                return CommonResult.success("删除转账充值列表成功")
                        .put("list", transferId)
                        .put("salesman", userRealName);
            } else {
                return CommonResult.error("删除转账充值列表失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

