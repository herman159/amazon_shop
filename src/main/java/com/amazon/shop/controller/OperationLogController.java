package com.amazon.shop.controller;


import com.amazon.shop.entity.OperationLog;
import com.amazon.shop.service.OperationLogService;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.OperationLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/operation-log")
@Api(tags = "日志模块")
public class OperationLogController {
    @Resource
    private OperationLogService operationLogService;

    /**
     * 查询日志列表
     *
     * @return
     */
    @GetMapping("/getOperationLog")
    @ApiOperation(value = "查询日志列表")
    private CommonResult getOperationLog() {
        List<OperationLog> operationLogs = operationLogService.list();
        List<OperationLogVo> operationLogVos = operationLogs.stream().map((operationLog) -> {
            OperationLogVo operationLogVo = new OperationLogVo();
            BeanUtils.copyProperties(operationLog, operationLogVo);
            return operationLogVo;
        }).collect(Collectors.toList());
        return CommonResult.success("查询日志列表成功").put("operationLogVos", operationLogVos);
    }
}

