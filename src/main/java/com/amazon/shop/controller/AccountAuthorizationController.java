package com.amazon.shop.controller;


import com.amazon.shop.aop.SystemLog;
import com.amazon.shop.constant.ReturnConstant;
import com.amazon.shop.entity.AccountAuthorization;
import com.amazon.shop.exception.CustomException;
import com.amazon.shop.service.AccountAuthorizationService;
import com.amazon.shop.service.AmazonAuthorityService;
import com.amazon.shop.service.impl.PermissionsServiceImpl;
import com.amazon.shop.service.impl.UsersServiceImpl;
import com.amazon.shop.utils.CommonResult;
import com.amazon.shop.vo.AccountAuthorizationSaveVo;
import com.amazon.shop.vo.AccountAuthorizationVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 郭非
 * @since 2021-06-12
 */
@RestController
@RequestMapping("/shop/account-authorization")
@Api(tags = "账户授权模块")
public class AccountAuthorizationController {
    @Resource
    private AccountAuthorizationService accountAuthorizationService;
    @Resource
    private UsersServiceImpl usersService;
    @Resource
    private PermissionsServiceImpl permissionsService;
    @Resource
    private AmazonAuthorityService amazonAuthorityService;

    /**
     * 保存账户授权信息
     *
     * @param authorizationVo
     * @return
     */
    @SystemLog(value = "新增店铺")
    @PostMapping("/saveAccountAuthorization")
    @ApiOperation(value = "新增店铺")
    @Transactional
    public CommonResult saveAccountAuthorization(@RequestBody AccountAuthorizationSaveVo authorizationVo, String token) {
        boolean flag = permissionsService.getPermissionList(token, "25");
        if (flag) {
            String id = accountAuthorizationService.saveAccountAuthorization(authorizationVo);
            String userRealName = usersService.getUserRealName(token);

            return CommonResult.success("保存账户授权信息成功")
                    .put("list", id)
                    .put("salesman", userRealName);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 查询店铺列表
     *
     * @param token
     * @return
     */
    @GetMapping("/getAccountAuthorization")
    @ApiOperation(value = "查询店铺列表")
    public CommonResult getAccountAuthorization(String token) {
        boolean flag = permissionsService.getPermissionList(token, "24");
        if (flag) {
            List<AccountAuthorization> accountAuthorizationList = accountAuthorizationService.list();
            if (accountAuthorizationList.size() > 0) {
                return CommonResult.success("查询店铺列表成功")
                        .put("accountList", accountAuthorizationList);
            } else {
                return CommonResult.error("查询店铺列表失败");
            }
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 删除店铺信息
     *
     * @param id
     * @return
     */
    @SystemLog(value = "删除店铺信息")
    @PostMapping("/removeAccountAuthorization")
    @ApiOperation(value = "删除店铺信息")
    @Transactional
    public CommonResult removeAccountAuthorization(String id, String token) {
        boolean flag = permissionsService.getPermissionList(token, "24");
        if (flag) {
            //根据店铺编号删除店铺信息
            accountAuthorizationService.removeById(id);

            //根据店铺编号删除授权信息
            amazonAuthorityService.removeById(id);

            //根据token查询业务员信息
            String userRealName = usersService.getUserRealName(token);
            return CommonResult.success("删除店铺信息成功")
                    .put("list", id)
                    .put("salesman", userRealName);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }

    /**
     * 查看店铺具体信息
     *
     * @param id
     * @param token
     * @return
     */
    @GetMapping("/getAccountAuthorizationById")
    @ApiOperation(value = "查看店铺具体信息")
    public CommonResult getAccountAuthorizationById(String id, String token) {
        boolean flag = permissionsService.getPermissionList(token, "24");
        if (flag) {

            //对应属性对拷
            AccountAuthorization authorization = accountAuthorizationService.getById(id);
            AccountAuthorizationVo accountAuthorizationVo = new AccountAuthorizationVo();
            BeanUtils.copyProperties(authorization, accountAuthorizationVo);

            //设置最后一次的更新时间
            String gmtModified = authorization.getGmtModified();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parse = null;
            try {
                parse = dateFormat.parse(gmtModified);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            accountAuthorizationVo.setLastSubmitTime(parse);
            return CommonResult.success().put("accountAuth", accountAuthorizationVo);
        }
        throw new CustomException(ReturnConstant.NO_PERMISSION_CODE
                , ReturnConstant.NO_PERMISSION_MESSAGE);
    }
}

