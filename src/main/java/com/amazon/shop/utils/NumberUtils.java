package com.amazon.shop.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * 数字工具类
 *
 * @Author 郭非
 * @Date 2021/6/14
 * @Time 19:54
 * @Version 1.0
 */
@Component
public final class NumberUtils {

    /**
     * 生成六位随机数
     *
     * @return
     */
    @Bean
    public String getSixRandomNumber() {
        Random random = new Random();
        String result = "";
        for (int i = 0; i < 6; i++) {
            result += random.nextInt(10);
        }
        return result;
    }

    /**
     * 生成十六位随机数
     *
     * @return
     */
    @Bean
    public String getSixteenRandomNumber() {
        Random random = new Random();
        String result = "";
        for (int i = 0; i < 16; i++) {
            result += random.nextInt(10);
        }
        return result;
    }
}
