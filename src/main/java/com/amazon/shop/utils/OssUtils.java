package com.amazon.shop.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.net.URI;

/**
 * Oss工具类
 *
 * @Author 郭非
 * @Date 2021/6/14
 * @Time 14:28
 * @Version 1.0
 */
@Component
public final class OssUtils {

    //读取配置文件内容
    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint;
    @Value("${aliyun.oss.file.keyid}")
    private String accessKeyId;
    @Value("${aliyun.oss.file.keysecret}")
    private String accessKeySecret;
    @Value("${aliyun.oss.file.bucketname}")
    private String bucketName;
    @Resource
    private NumberUtils numberUtils;

    /**
     * 网络流上传文件
     *
     * @param picPath
     * @return
     */
    public String uploadOssWebFile(String productId, String picPath) {

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {
            String randomNumber = numberUtils.getSixRandomNumber();
            //生成以时间为标准的文件
            String datePath = new DateTime().toString("yyyy/MM/dd/");
            //利用产品名生成图片名
            String picName = datePath + randomNumber + productId + ".jpg";
            //获取网络流
            InputStream inputStream = new URI(picPath).toURL().openStream();
            // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
            ossClient.putObject(bucketName, picName, inputStream);
            // 关闭OSSClient。
            ossClient.shutdown();
            //返回上传文件路径
            String url = "https://" + bucketName + "." + endpoint + "/" + picName;
            return url;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 文件流上传本地文件
     *
     * @param productId
     * @return
     */
    public String uploadOssLocalFile(String productId, MultipartFile multipartFile) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {
            // 获取上传文件输入流
            InputStream inputStream = multipartFile.getInputStream();
            //获取当前日期
            String datePath = new DateTime().toString("yyyy/MM/dd/");
            //拼接成唯一文件名
            String picName = datePath + productId + ".jpg";
            // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
            ossClient.putObject(bucketName, picName, inputStream);
            // 关闭OSSClient。
            ossClient.shutdown();
            //返回上传文件路径
            String url = "https://" + bucketName + "." + endpoint + "/" + picName;
            return url;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
