package com.amazon.shop.utils;

import com.amazon.shop.vo.AddressVo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


/**
 * 根据ip获取地址
 *
 * @author 郭非
 */
public class AreaUtils {
    public static AddressVo getAddressByIp(String ip, String ak) {
        AddressVo addressVo = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<AddressVo> forEntity = restTemplate
                    .getForEntity("http://api.map.baidu.com/location/ip?"
                            + "ak=" + ak + "&ip=" + ip, AddressVo.class);
            addressVo = forEntity.getBody();
        }catch (Exception e){
            e.printStackTrace();
        }
        return addressVo;
    }
}
