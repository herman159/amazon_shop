package com.amazon.shop.utils;

import com.amazon.shop.vo.CodeRate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @Author 郭非
 * @Date 2021/7/4
 * @Time 12:42
 * @Version 1.0
 */
public class RateUtils {
    /**
     * 获取实时汇率
     *
     * @param fromCode
     * @param toCode
     * @return
     */
    public static Double exchangeMoney(String fromCode, String toCode) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CodeRate> codeRateResponseEntity = restTemplate.getForEntity("https://api.it120.cc/gooking/forex/rate?fromCode="
                + fromCode + "&toCode=" + toCode, CodeRate.class);
        CodeRate codeRate = codeRateResponseEntity.getBody();
        Double rate = codeRate.getData().getRate();
        return rate;
    }
}
