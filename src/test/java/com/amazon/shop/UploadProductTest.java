package com.amazon.shop;

import com.amazon.shop.api.GetFeed;
import com.amazon.shop.entity.AmazonAuthority;
import com.amazon.shop.spapi.api.FeedsApi;
import com.amazon.shop.spapi.client.ApiException;
import com.amazon.shop.spapi.documents.exception.CryptoException;
import com.amazon.shop.spapi.documents.exception.HttpResponseException;
import com.amazon.shop.spapi.model.feeds.*;
import com.amazon.shop.utils.XMLUtils;
import com.amazon.shop.vo.ProductTypeVo;
import com.amazon.shop.vo.ProductUploadVo;
import org.threeten.bp.OffsetDateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 产品上传测试类
 *
 * @Author 郭非
 * @Date 2021/6/24
 * @Time 21:07
 * @Version 1.0
 */
public class UploadProductTest {
    public static void main(String[] args) throws IOException, ApiException, HttpResponseException, CryptoException {
        //设置授权信息
        AmazonAuthority amazonAuthority = new AmazonAuthority();
        amazonAuthority.setId("db09b7438fdb4a5463b208304ddd727c");
        amazonAuthority.setAccessKeyId("AKIAUAUZA6N3YDNRZMWS");
        amazonAuthority.setSecretKey("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");
        amazonAuthority.setRegion("us-east-1");
        amazonAuthority.setRoleArn("arn:aws:iam::276272575351:role/SellingPartnerAPIRole");
        amazonAuthority.setRoleSessionName("276272575351");
        amazonAuthority.setClientId("amzn1.application-oa2-client.dcaae6f7d4174b41b13bf853b5147f04");
        amazonAuthority.setClientSecret("be4bff5cf707b791286ca1cc796d9583346d17ae0a8b48c203743aaccf1ba9a4");
        amazonAuthority.setRefreshToken("Atzr|IwEBIBdI0FrG0GI6nu-n6GP4sP7f4ppBD2G_2FZrwjyOIiF0mIJV7Mu0ZlZvfz3EpOPUDLLd3I391U1-DZUIJU8V8O0ObufCF4ssOi9e2qJFzaTzTl1VwuLv7tMooBz7FzXrHVS9jcmH6SNhQrMfda4X9t75ZyLjk_Aism1PApRe5f4OUNIuYDIUfbgPMniU6XvCQvpfxg0lB32SIXmDEbBhaWMqD33Eq-bXRgTkt1JrEaWZLxLu36SnkAslugctSTySS3oud8gZ9aBvU9z4QT_KSw_q0Wlxg0LmwW6AOAvPon4xX-YjVDQeZT6kCvm5ic9c4zw");
        amazonAuthority.setSpEndPoint("https://sellingpartnerapi-na.amazon.com");
        amazonAuthority.setLwaEndPoint("https://api.amazon.com/auth/o2/token");

        List<String> marketplaceIds = new ArrayList<>();
        marketplaceIds.add("ATVPDKIKX0DER");

        //第一步
        String contentType = "text/plain; charset=UTF-8";
        Feed.ProcessingStatusEnum processingStatus = null;
        OffsetDateTime processingEndTime = null;
//        try {
//            FeedsApi feedsExample = GetFeed.getFeedsExample(amazonAuthority);
//            CreateFeedDocumentSpecification createFeedDocumentSpecification = new CreateFeedDocumentSpecification();
//            createFeedDocumentSpecification.setContentType(contentType);
//            CreateFeedDocumentResponse feedDocument = feedsExample.createFeedDocument(createFeedDocumentSpecification);
//            String feedDocumentId = feedDocument.getPayload().getFeedDocumentId();
//            System.out.println("*************" + feedDocumentId + " ***********");
//            String key = feedDocument.getPayload().getEncryptionDetails().getKey();
//            String initializationVector = feedDocument
//                    .getPayload()
//                    .getEncryptionDetails()
//                    .getInitializationVector();
//            String url = feedDocument.getPayload().getUrl();

            //第二步
//            List<ProductUploadVo> productUploadVoList = new ArrayList<>();
//            ProductUploadVo productUploadVo = new ProductUploadVo();
//
//            productUploadVo.setTitle("Countertop Wine Rack, Iron Wine Bottle Rack, Can Hold 6 Standard Wine Bottles,Street Style Decorations Home Creative Wine Cabinet Decoration");
//            productUploadVo.setSearchTerms("Wine Racks Countertop Small Wine Cabinet Vintage Home Decor Industrial Decor Steampunk Decor Copper Home Decor Cast Iron Rack Iron Wine Rack Cast Iron Storage Rack Liquor Cabinet Liquor Rack Liquor Shelves Counter Wine Rack Men Gift");
//            productUploadVo.setDescription("Product description\n" +
//                    "Material: Iron\n" +
//                    "Size: 52*32.5/22.5cm\n" +
//                    "Weight: 1384 grams\n" +
//                    "Our wine rack design concept is practical and safe, yet simple and beautiful. Choose a safe wine rack to store wine instead of high-end wine.\n" +
//                    "The product can be used as furniture, real estate decoration, and can also be used as a gift to relatives, friends and customers. Suitable for any scene.\n" +
//                    "The wine rack is flat, simple, beautiful and strong.\n" +
//                    "Our products only contain wine racks");
//            productUploadVo.setBulletPoint("Introduction: Our wine rack design concept is practical and safe, simple and beautiful. Choosing a safe wine rack to store quality wines will give you peace of mind.\n" +
//                    "Use place: The simple and beautiful wine rack is suitable for any place, not only for placing wine, but also for decoration.\n" +
//                    "Others: Due to the differences in the brightness and contrast of personal displays, there may be slight differences between the picture and the actual product. Due to different measurement methods, any error of 2cm is within a reasonable range. [Listed price, unit price of wine rack, excluding wine glass and wine bottle]\n" +
//                    "Can hold six standard wine bottles-six standard bottled wine, beer, spirits or cocktail shakers can fit into the radial face of this unique wine rack.\n" +
//                    "Convenient countertop-this wine rack can be easily installed on the counter of any home kitchen, the top of the wine cabinet, or even the bar cart.");
//            productUploadVo.setBrand("");
//            productUploadVo.setMfrPartNumber("");
//            productUploadVo.setManufacturer("");
//            productUploadVo.setSku("641553682763-1");
//            productUploadVo.setOperationType("Update");
//
//            ProductTypeVo productTypeVo = new ProductTypeVo();
//            productTypeVo.setProductTypeOne("家具厨房用品");
//            productTypeVo.setProductTypeTwo("厨房和餐厅");
//            productTypeVo.setProductTypeThree("酒类配件");
//            productTypeVo.setProductTypeFour("酒架");
//            productTypeVo.setProductTypeFive("酒具套装");
//            productTypeVo.setProductTypeSix("");
//            productUploadVo.setProductTypeVo(productTypeVo);
//            productUploadVoList.add(productUploadVo);
//
////            File xml = XMLUtils.createXML(productUploadVoList);
////            FileInputStream fileInputStream = new FileInputStream(xml);
//
////            byte[] xmlByte = new byte[fileInputStream.available()];
////            fileInputStream.read(xmlByte);
////            fileInputStream.close();
////            String xmlData = xmlByte.toString();
//            File file = new File("F:\\xml\\test.xml");
//            FileInputStream fileInputStream = new FileInputStream(file);
//            byte[] upload = new byte[fileInputStream.available()];
//            fileInputStream.read(upload);
////            String xmlData = upload.toString();
//            String xmlData = new String(upload, "UTF-8");
//            UploadExample uploadExample = new UploadExample();
//            uploadExample.encryptAndUpload_fromPipedInputStream(key, initializationVector, url, xmlData);
//
//            //第三步
//            CreateFeedSpecification createFeedSpecification = new CreateFeedSpecification();
//            createFeedSpecification.setFeedType("POST_PRODUCT_DATA");
////            createFeedSpecification.setFeedOptions(null);
//            createFeedSpecification.setMarketplaceIds(marketplaceIds);
//            createFeedSpecification.setInputFeedDocumentId(feedDocumentId);
//
//            CreateFeedResponse feed = feedsExample.createFeed(createFeedSpecification);
//            String feedId = feed.getPayload().getFeedId();
//            //第四步
//            GetFeedResponse response = feedsExample.getFeed(feedId);
////            System.out.println(exampleFeedDocument + "-------------Document");
////            processingStatus = response.getPayload().getProcessingStatus();
////            processingEndTime = response.getPayload().getProcessingEndTime();
//            System.out.println(response + "-----------Response");
//        } catch (ApiException e) {
//            System.out.println(e.getResponseHeaders() + "------------");
//            System.out.println(e.getResponseBody() + "------------");
//            e.printStackTrace();
//        }
        //feedId 为第三步得到
            FeedsApi feedsExample = GetFeed.getFeedsExample(amazonAuthority);
            GetFeedResponse feed = feedsExample.getFeed("54434018811");
//            System.out.println(feed);
//
//////            //feedDocumentId 为上一步得到
        GetFeedDocumentResponse feedDocument = feedsExample
                .getFeedDocument("amzn1.tortuga.3.636d6221-4508-4a57-bf0c-d630e2e1f5dd.TICS8Q0XZWUQF");
        System.out.println(feedDocument);
//
//        //        key initializationVector url compressionAlgorithm 参数为上一步得到
        DownloadExample downloadExample = new DownloadExample();
        String key = "tU6Bikcbxwr3daGDzk+QkC19lEj84TOgVmqK9//J11s=";
        String initializationVector = "u+qoYUqQDJ9DnUQU+mCwvQ==";
        String url = "https://tortuga-prod-na.s3-external-1.amazonaws.com/%2FNinetyDays/amzn1.tortuga.3.636d6221-4508-4a57-bf0c-d630e2e1f5dd.TICS8Q0XZWUQF?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20210703T081243Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=AKIA5U6MO6RAKE6ZSV4V%2F20210703%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=ee3894b3e1bed4d52e61d2a1fb5d013e225534e57a4df6e9f68377a78cba583a";
        String compressionAlgorithm = null;
        downloadExample.downloadAndDecrypt(key, initializationVector, url, null);
    }
}
