package com.amazon.shop;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.amazon.shop.spapi.client.ApiException;
import com.amazon.shop.utils.XMLUtils;
import com.amazon.shop.vo.ProductUploadVo;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

//@SpringBootTest
class AmazonShopApplicationTests {

    @Test
    void contextLoads() {
        // 1、创建代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 2、全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        //工程绝对路径 + 工程的类路径
        gc.setOutputDir("C:\\IntelliJ IDEA 2020.3\\amazon_shop" + "/src/main/java");
        gc.setAuthor("郭非");
        gc.setOpen(false); //生成后是否打开资源管理器
        gc.setFileOverride(true); //重新生成时文件是否覆盖
        gc.setServiceName("%sService");    //去掉Service接口的首字母I
        gc.setIdType(IdType.ASSIGN_UUID); //主键策略
        gc.setDateType(DateType.ONLY_DATE);//定义生成的实体类中日期类型
        gc.setSwagger2(true);//开启Swagger2模式

        mpg.setGlobalConfig(gc);

        // 3、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://1.117.139.9:3306/shop?characterEncoding=utf8&useUnicode=true&useSSL=false&serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 4、包配置
        PackageConfig pc = new PackageConfig();
        //生成之后的包结构为: com.museum.account
        pc.setModuleName("shop"); //模块名
        pc.setParent("com.amazon");
        pc.setController("controller");
        pc.setEntity("entity");
        pc.setService("service");
        pc.setMapper("mapper");
        mpg.setPackageInfo(pc);

        // 5、策略配置
        StrategyConfig strategy = new StrategyConfig();
        //表名
        strategy.setInclude("message_status");
        strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
        strategy.setTablePrefix(pc.getModuleName() + "_"); //生成实体时去掉表前缀

        strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
        strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作

        strategy.setRestControllerStyle(true); //restful api风格控制器
        strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符

        mpg.setStrategy(strategy);
        // 6、执行
        mpg.execute();
    }

    /**
     * oss上传测试
     *
     * @return
     */
    public static void main(String[] args) {
        String endpoint = "oss-cn-beijing.aliyuncs.com";
        String accessKeyId = "LTAI5t8Kf8TTaSTA1HmoCgse";
        String accessKeySecret = "ckOHZFwm5cHaesbD3RJ1UALlL9nu56";
        String bucketName = "amazon-shop";
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {
            InputStream inputStream =
                    new URI("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fcdn.duitang.com%2Fuploads%2Fitem%2F201410%2F04%2F20141004172507_J8Mty.jpeg&refer=http%3A%2F%2Fcdn.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1626251380&t=8dd036ff303ba46689bbc5bf88488441")
                            .toURL()
                            .openStream();
            //利用产品名生成图片名
            String picName = UUID.randomUUID().toString().replace("-", "") + "pic" + ".jpg";
            // 填写Bucket名称和Object完整路径。Object完整路径中不能包含Bucket名称。
            ossClient.putObject(bucketName, picName, inputStream);
            // 关闭OSSClient。
            ossClient.shutdown();
            //返回上传文件路径
            String url = "https://" + bucketName + "." + endpoint + "/" + picName;
            System.out.println(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInteger() throws ParserConfigurationException, TransformerException, ApiException, FileNotFoundException {
        List<ProductUploadVo> productUploadVoList = new ArrayList<>();
        ProductUploadVo productUploadVo = new ProductUploadVo();

        productUploadVo.setTitle("Countertop Wine Rack, Iron Wine Bottle Rack, Can Hold 6 Standard Wine Bottles,Street Style Decorations Home Creative Wine Cabinet Decoration");
        productUploadVo.setDescription("Product description\n" +
                "Material: Iron\n" +
                "Size: 52*32.5/22.5cm\n" +
                "Weight: 1384 grams\n" +
                "Our wine rack design concept is practical and safe, yet simple and beautiful. Choose a safe wine rack to store wine instead of high-end wine.\n" +
                "The product can be used as furniture, real estate decoration, and can also be used as a gift to relatives, friends and customers. Suitable for any scene.\n" +
                "The wine rack is flat, simple, beautiful and strong.\n" +
                "Our products only contain wine racks");
        productUploadVo.setBulletPoint("Introduction: Our wine rack design concept is practical and safe, simple and beautiful. Choosing a safe wine rack to store quality wines will give you peace of mind.\n" +
                "Use place: The simple and beautiful wine rack is suitable for any place, not only for placing wine, but also for decoration.\n" +
                "Others: Due to the differences in the brightness and contrast of personal displays, there may be slight differences between the picture and the actual product. Due to different measurement methods, any error of 2cm is within a reasonable range. [Listed price, unit price of wine rack, excluding wine glass and wine bottle]\n" +
                "Can hold six standard wine bottles-six standard bottled wine, beer, spirits or cocktail shakers can fit into the radial face of this unique wine rack.\n" +
                "Convenient countertop-this wine rack can be easily installed on the counter of any home kitchen, the top of the wine cabinet, or even the bar cart.");
        productUploadVo.setBrand("1");
        productUploadVo.setMfrPartNumber("1");
        productUploadVo.setManufacturer("1");
        productUploadVo.setSku("641553682763-1");
        productUploadVo.setOperationType("Update");
        productUploadVo.setRecommendedBrowseNode("1");
        productUploadVo.setThreadCount("1");
        productUploadVo.setType("1");
        productUploadVo.setValue("1");
        productUploadVo.setMessageId("1");
        productUploadVo.setVariationTheme("1");
        productUploadVo.setMaterial("1");
        productUploadVo.setConditionType("1");
        productUploadVoList.add(productUploadVo);


        File xml = XMLUtils.createXML(productUploadVoList);
        System.out.println(xml);
//        FileInputStream fileInputStream = new FileInputStream(xml);

    }


}
