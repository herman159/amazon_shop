package com.amazon.shop;

import com.amazon.shop.spapi.documents.CompressionAlgorithm;
import com.amazon.shop.spapi.documents.DownloadBundle;
import com.amazon.shop.spapi.documents.DownloadHelper;
import com.amazon.shop.spapi.documents.DownloadSpecification;
import com.amazon.shop.spapi.documents.exception.CryptoException;
import com.amazon.shop.spapi.documents.exception.HttpResponseException;
import com.amazon.shop.spapi.documents.exception.MissingCharsetException;
import com.amazon.shop.spapi.documents.impl.AESCryptoStreamFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/**
 * @Author 郭非
 * @Date 2021/6/25
 * @Time 19:22
 * @Version 1.0
 */
public class DownloadExample {
    final DownloadHelper downloadHelper = new DownloadHelper.Builder().build();

    // key, initializationVector, url, and compressionAlgorithm are returned by the getReportDocument operation.
    public void downloadAndDecrypt(String key, String initializationVector, String url, String compressionAlgorithm) {
        AESCryptoStreamFactory aesCryptoStreamFactory =
                new AESCryptoStreamFactory.Builder(key, initializationVector).build();

        DownloadSpecification downloadSpec = new DownloadSpecification.Builder(aesCryptoStreamFactory, url)
                .withCompressionAlgorithm(CompressionAlgorithm.fromEquivalent(compressionAlgorithm))
                .build();

        try (DownloadBundle downloadBundle = downloadHelper.download(downloadSpec)) {
            System.out.println(downloadBundle  + "******");
            // This example assumes that the downloaded file has a charset in the content type, e.g.
            // text/plain; charset=UTF-8
            try (BufferedReader reader = downloadBundle.newBufferedReader()) {
                String line;
                do {
                    line = reader.readLine();
                    System.out.println(line);
                    // Process the decrypted line.
                } while (line != null);
            }
        } catch (CryptoException | IOException | MissingCharsetException | HttpResponseException e) {
            e.printStackTrace();
        }
    }
}
