package com.amazon.shop;

import com.amazon.shop.spapi.documents.UploadHelper;
import com.amazon.shop.spapi.documents.UploadSpecification;
import com.amazon.shop.spapi.documents.exception.CryptoException;
import com.amazon.shop.spapi.documents.exception.HttpResponseException;
import com.amazon.shop.spapi.documents.impl.AESCryptoStreamFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @Author 郭非
 * @Date 2021/6/25
 * @Time 16:43
 * @Version 1.0
 */
public class UploadExample {
    private final UploadHelper uploadHelper = new UploadHelper.Builder().build();

    // key, initializationVector, and url are returned by the createFeedDocument operation.
    public void encryptAndUpload_fromString(String key, String initializationVector, String url) {
        AESCryptoStreamFactory aesCryptoStreamFactory =
                new AESCryptoStreamFactory.Builder(key, initializationVector)
                        .build();

        // This contentType must be the same value that was provided to createFeedDocument.
        String contentType = String.format("text/plain; charset=%s", StandardCharsets.UTF_8);

        // The character set must be the same one that is specified in contentType.
        try
                (InputStream source = new ByteArrayInputStream("my feed data".getBytes(StandardCharsets.UTF_8))) {
            UploadSpecification uploadSpec =
                    new UploadSpecification.Builder(contentType, aesCryptoStreamFactory, source, url)
                            .build();

            uploadHelper.upload(uploadSpec);
        } catch (CryptoException | HttpResponseException | IOException e) {
            // Handle exception.
        }
    }

    // key, initializationVector, and url are returned from createFeedDocument.
    public void encryptAndUpload_fromPipedInputStream(String key, String initializationVector, String url,String s) {
        AESCryptoStreamFactory aesCryptoStreamFactory =
                new AESCryptoStreamFactory.Builder(key, initializationVector)
                        .build();

        // This contentType must be the same value that was provided to createFeedDocument.
        String contentType = String.format("text/plain; charset=%s", StandardCharsets.UTF_8);
        try
                (PipedInputStream source = new PipedInputStream()) {
            new Thread(
                    new Runnable() {
                        public void run() {
                            try
                                    (PipedOutputStream feedContents = new PipedOutputStream(source)) {
                                // The character set must be the same one that is specified in contentType.
                                feedContents.write(s.getBytes(StandardCharsets.UTF_8));
//                                feedContents.write(s.getBytes(StandardCharsets.UTF_8));
                            } catch (IOException e) {
                                // Handle exception.
                            }
                        }
                    }).start();

            UploadSpecification uploadSpec =
                    new UploadSpecification.Builder(contentType, aesCryptoStreamFactory, source, url)
                            .build();

            uploadHelper.upload(uploadSpec);
        } catch (CryptoException | HttpResponseException | IOException e) {
            // Handle exception.
        }
    }
}
